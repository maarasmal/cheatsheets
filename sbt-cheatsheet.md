# Stupid SBT tricks
Some of these are duplicates of gems that I found [here](https://underscore.io/blog/posts/2015/11/09/sbt-commands.html)

There are some good general tips about SBT, such as where to put configuration files, etc.

[SBT by example](https://www.scala-sbt.org/1.x/docs/sbt-by-example.html)

## Set up a new project from scratch

This is similar to the `mvn archetype` command. See the
[SBT docs](https://www.scala-sbt.org/1.x/docs/sbt-new-and-Templates.html) for details.

For the most bare-bones basic project, suitable for just adding code and unit tests:

```
sbt new scala/hello-world.g8
```

The SBT doc link above references [this page](https://github.com/foundweekends/giter8/wiki/giter8-templates)
that contains even more giter8 templates.

## Very True Things
Quoting Robert Fries from Slack chat in 2019:
> Robert Fries [9:55 AM] yeah, one downside of sbt is that it either works or there goes the rest of the day

## Run just one test class
```
testOnly mypackage.FooTest
testOnly *FooTest
```
To only run some test function(s), you can also do this:
```
testOnly *FooTest -- -z "Component test that"
```
Any tests whose name matches the string will be run. So if you have the following in `FooTest`:
```
test("FeatureA - Verifies a thing") { ??? }
test("FeatureA - Verifies that error case X is handled") { ??? }
test("FeatureB - Verifies a thing") { ??? }
test("FeatureB - Verifies that error case Y is handled") { ??? }
```
You might conceivably do things like the following, each of which should run 2 tests:
```
testOnly *FooTest -- -z "FeatureA"
testOnly *FooTest -- -z "error case"
```
The first command runs the first two tests, the second command runs the 2nd and 4th tests.

## Enable Ctrl+C to kill a build without exiting SBT
Add the following to `~/.sbt/<version>/global.sbt`:
```
// Allow usage of Ctrl+C to kill things without exiting SBT itself
cancelable in Global := true
```

## Disable warnings on the SBT console for unused imports
Add this to your `build.sbt` to resolve the annoying case where you start the SBT console and get
warnings every time you type in an `import` statement:
```
scalacOptions in (Compile, console) ++= Seq("-Ywarn-unused:-imports")
```

---

# Ammonite
Ammonite != SBT, but it's in the ballpark, and I don't think it needs its own cheatsheet (yet)

## Common imports I want for Ammonite
I can never remember the exact syntax for importing dependency jars:
```
import $ivy.`org.typelevel::cats-effect:1.0.0-RC2`
import $ivy.`org.scodec::scodec-bits:1.1.2`
```

## Auto-import dependencies on startup of Ammonite
Add the following in `~/.ammonite/predef.sc`:
```
println("Importing scodec-bits...")
import $ivy.`org.scodec::scodec-bits:1.1.2`

println("Importing fs2-core...")
import $ivy.`co.fs2::fs2-core:0.10.1`

println("Importing cats-effect...")
import $ivy.`org.typelevel::cats-effect:1.0.0-RC2`
```
