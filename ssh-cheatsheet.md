# SSH tips and tricks

[man 5 ssh-config](http://man.openbsd.org/cgi-bin/man.cgi/OpenBSD-current/man5/ssh_config.5?query=ssh%5fconfig&arch=i386)

| Command | Description |
|---|---|
| `eval $(ssh-agent)` | Starts the `ssh-agent` process and sets environment variables used by other ssh tools to locate it |
| `ssh-add` | Loads the key found in `$HOME/.ssh/id_rsa` and prompts you for its password |
| `ssh-add ~/.ssh/id_rsa-other` | Loads the key found in `~/.ssh/id_rsa-other` and prompts you for its password |
| `ssh-add -D` | Delete all identities from the agent |
| `ssh-agent -k` | Terminate the agent. This relies on the agent having been started via the `eval` command above |
| `ssh-keygen -L -f ~/.ssh/id_rsa-cert.pub` | Print cert details. Useful if using something like Autobahn to authenticate via a bastion host |

In a GUI-based environment, the SSH agent is often configured to run automatically when you start
a GUI session. How this is configured depends on the OS. So there, you likely just need to log in
to a GUI session, open a terminal and run `ssh-add` to get your key loaded.

When you SSH in to a server, even if it has a GUI, the agent is not started for your login shell.
So there you must run `eval $(ssh-agent)` to start the agent and initialize environment variables
that `ssh-add` will use to know where to load your key. When you log out, you should terminate the
agent that you started via `ssh-agent -k`. You might want to add an alias like the following to
do this cleanup automatically:

```
alias logout='ssh-agent -k; builtin logout'
```

You don't want to use `&&` to join the commands because if there is no agent running, you won't be
logged out.

## About keeping your session active
When you login to a remote host, it might be configured to log out accounts after a certain idle
timeout. On the remote host, this can often be controlled with the `TMOUT` environment variable:
```
export TMOUT=0  # disables the idle timeout
export TMOUT=3600  # Timeout after 1 hour idle (this value is in seconds)
```
See `man bash` and search for `TMOUT` for more detail.

If the remote host logs you out due to hitting the `TMOUT` value, you will usually see a message
like `timed out waiting for input: auto-logout`. Your own SSH client can also kill the connection
if it is idle as well. In this case, you will often return to your session and find it "frozen" for
a moment, and then you will see a message about a `broken pipe` when it un-sticks. The SSH client
can be configured to send "keep-alive" messages periodically.

As an example, you can place the following in your `~/.ssh/config` file:
```
Host *
  ServerAliveInterval 120
  ServerAliveCountMax 5
```

This tells the client to send a "keep-alive" message every 120 seconds, and to do so up to 5 times.
Thus, this will keep your connection active for about 10 minutes at a minimum.

For more details, see [this post](https://unix.stackexchange.com/questions/3026/what-options-serveraliveinterval-and-clientaliveinterval-in-sshd-config-exac).

There are corresponding settings that can be applied on the server in `/etc/ssh/sshd_config`:
```
# Send null packets to clients every 120 seconds, up to 15 times. That is, keep connections alive
# for up to 30 minutes.
ClientAliveInterval 120
ClientAliveCountMax 15
```

## Preventing "too many authentication failures" errors

The `-o IdentitiesOnly=yes` option here tells SSH to only attempt to log in with the key specified
by the `-i` option. This should prevent a "too many authentication failures" error that can occur
if you have multiple identities loaded in your ssh-agent. See articles such as
[this one](https://superuser.com/a/187790) for details.

```
ssh -i path/to/ssh_id -o IdentitiesOnly=yes myuser@myhost
```

## Miscellaneous

[This post](https://blog.scottlowe.org/2015/12/24/running-ansible-through-ssh-bastion-host/) was
sent to me while working through authentication issues involving Ansible, SSH and a bastion host
setup at Comcast. Wasn't directly applicable, but did have some helpful general knowledge about
SSH configurations.

