# Handy Git commands

[Here](https://grrr.tech/posts/2022/master-to-main/) is one of the better writeups that I have
found about switching your existing `master` branch to be named `main`.

## Getting Started (cloning, creating branches)
| Command | Description |
|---|---|
| `git clone https://github.com/scodec/scodec.git` | Clone a repository. Optionally include a directory name after the URL if you want the checkout to go into a directory other than the one named by the URL (in this example, it will default to `scodec`, but if you added ` foo` to the end of this command, the resulting directory w/ the scodec repository in it will be called `foo`)
| `git checkout mybranch` | Switch to the branch named `mybranch` |
| `git checkout -b mybranch sometag` | Create a new branch named `mybranch` from the commit identified by `sometag` |
| `git clone https://github.com/scodec/scodec.git -b series/1.9.x foo/scodec` | Clones the scodec repo into the `./foo/scodec/` directory and sets the initial branch to `series/1.9.x` |


## Standard workflow type commands
| Command | Description |
|---|---|
| `git fetch origin` | Retrieve the latest data from the remote named `origin`. No updates are made to any local branches. |
| `git merge origin/master` | Merge the current state of `origin/master` into your current branch. Note that this branch may be out of data if you have not recently performed a `git fetch origin`. |
| `git checkout -b newbranch` | Create a new branch from the tip of the current branch you are on, named `newbranch` |
| `git diff -w` | Show unstaged modifications, ignoring whitespace |
| `git checkout -- somefile.c` | Discard all modifications to `somefile.c`, restoring the file to its state in the last commit in the current branch |
| `git checkout -- foo/` | Discard all modifications to all modified files under the `foo/` directory, recursively. If run from the root of the repository with the directory `.`, this will discard all of your modifications to all files in the repository. |
| `git checkout 61b02af4 -- foo/bar/blah.scala` | Reset the content of `foo/bar/blah.scala` to its state as of commit ID `61b02af4` |
| `git add somefile.scala` | Add the file `somefile.scala` to Git so that it will track it. This stages the file too. |
| `git add foo/bar/` | Stages all modified files that reside under `foo/bar/`, recursively |
| `git commit -a` | Stages and commits all modified files, opening `$EDITOR` to edit the commit message. **NOTE:** Does not add new files |
| `git commit --amend` | Add changes to your previous commit. For instance, this might be useful if you forgot to add something to your previous commit, or if you realize you left some commented-out code in a file that you meant to delete. Just delete the dead code and run these commands to include those line deletions in your previous commit. |
| `git push fork newbranch` | Pushes `newbranch` up to the remote named `fork` |
| `git push --force fork newbranch:remotebranch` | Force push local branch `newbranch` to a branch named `remotebranch` on the remote `fork` |
| `git update-index --chmod=+x path/to/file` | Add the executable bit to a file that is already checked in. Typically you'd do this if you forgot to make a file executable before you added it to the repository. |

## Working with Branches
| Command | Description |
|---|---|
| `git branch -a` | Lists all branches, local and remote. Note that this does NOT go to the server for the branch names and will only reflect the branches present when you last did a fetch or merge. |
| `git branch -m oldname newname` | Rename the local branch `oldname` to `newname` |
| `git tag -a` | List all tags. Also does not go to the server, so this data could be stale. |
| `git checkout --track origin/remotebranch` | Create a local branch that will track the remote branch named `remotebranch`. |
| `git push origin :remotebranch` | Deletes the remote branch named `remotebranch`. Note the space between `origin` and the colon. |
| `git push origin --delete branchname` | Delete the branch `branchname` on the remote `origin`. Same as the row above, just a little cleaner to understand. |
| `git remote prune origin` | Cleans up local copies of remote tracking branches under `origin`. Use this if someone deleted a branch or tag on `origin` and you want to clean up your local repository to match. |
| `git merge --ff-only origin/master` | Merge `origin/master` into your current branch, but only do the merge if it can be a fast-forward (i.e., no merge commit is created) |
| `git cherry -v master mybranch` | List the commits that are in branch `mybranch` that have NOT been merged into `master` |
| `git merge origin/otherbranch` | Merge the your current copy of `origin/otherbranch` into your current branch. NOTE: if you have not done a `git fetch origin`, `origin/otherbranch` may not be up-to-date with the server. |
| `git branch --set-upstream-to=origin/foo foo` | Configures your local branch `foo` to track the branch `foo` on the remote `origin` |
| `git branch branch_name --set-upstream-to your_new_remote/branch_name` | Similar to the above, but changes the remote to `your_new_remote` as well. Useful if say, you clone a repo, create a local branch, then you fork and want to point your local branch at `fork/branch_name`. |
| `git merge --abort` | Attempts to reset your working copy to whatever state it was in before you started a merge |
| `git cherry-pick -x <hash>` | Apply the changes of commit `<hash>` to the current branch. Useful for porting bugfixes to other branches. Include the `-x` option to auto-include a bit in the commit message about where the commit was picked from, since the new commit will have a new commit ID |
| `git branch -m new-name` | Renames the current local branch to `new-name`. If you've already pushed the old branch to a remote, delete it on the remote, then push it, perhaps with the following command. |
| `git push origin -u branchname` | Push the local branch to the remote and update the upstream branch to refer to the remote branch. Especially useful after you have renamed a branch. |


## Remotes, forking, etc.

| Command | Description |
|---|---|
| `git remote` | List all of your currently defined remotes |
| `git remote add fork <url>` | Add a remote named `fork`, with the specified `<url>` (this is the "clone" URL you get from GitHub, Bitbucket, etc.) |
| `git remote show fork` | Print information about the remote named `fork`. Of note, this includes the URL for this remote |
| `git remote get-url fork` | Prints just the URL for the remote named `fork`. Doesn't ping the remote, but not supported on older versions of Git |
| `git remote remove origin && git remote add origin <URL>` | Deletes the remote named `origin` and adds it back with the specified URL. Typically you would do this if your repository is relocated or renamed. |

## Working With the Stash
Sometimes you have some edits that you don't want to commit, but you need to get out of the way temporarily so you can merge something else in, or make some other changes. Using the stash can be handy for that.  Most of the time, you would use the stash like a stack, probably just saving something to the stash, then popping it off, but you can stash multiple items and apply them back to your local directory in any order.

| Command | Description |
|---|---|
| `git stash save` | Saves off local edits to the stash at the top of the stack |
| `git stash pop` | Pops the last stash off of the stack and applies it to the current working tree |
| `git stash apply` | Applies the latest stash to the current working tree, but does not remove it from the stash list |
| `git stash list` | Lists the stashes you currently have saved.  The first item on each line is the stash name, and will have a format like `stash@{0}`. |
| `git stash pop stash@{2}` | Does the same as `git stash pop` above, but using the third stash in the list, rather than the most recent (which is always named `stash@{0}`). |

## Working With Tags
| Command | Description |
|---|---|
| `git tag -a foo-tag -m 'Some tag message'` | Creates a tag named `foo-tag` on the commit at the tip of your current local branch |
| `git push origin --tags` | Pushes all local tags to the remote named `origin` |
| `git push origin bar-tag` | Pushes just the tag named `bar-tag` to the remote named `origin` |
| `git tag -a foo-tag 6c1cbbc71f` | Create a tag named `foo-tag` on the commit `6c1cbbc71f`. Useful if you need to tag a commit after you have made other commits in the same branch. |
| `git show foo-tag` | Show information for the tag named `foo-tag`, including who created it, when it was created, etc. |
| `git tag -d sometag && git push origin :refs/tags/sometag` | Run both of these commands to delete a tag named `sometag` from the remote `origin`. **NOTE:** there is a space between `origin` and `:`. |

## Configuration Tricks
| Command | Description |
|---|---|
| `git config --global push.default upstream` | Tells git to only push the current branch to its upstream branch when you enter 'git push', rather than pushing all branches to their upstream branches (the default). See [the documentation for git-config](https://www.kernel.org/pub/software/scm/git/docs/v1.7.5.3/git-config.html) for more details. |
| `git config --global color.ui auto` | Tells git to use colors in most commands |
| `git config --global alias.lg 'log --date=local --pretty=format:"%h %C(yellow)%<(22,trunc)%ai%Creset %s - %Cblue%an%Creset"'` | This creates an alias for "git lg", which is just a color-formatted one-line-per-commit display of the log. |

# Backporting fixes to other branches
Sometimes we fix a bug on `main` but need to also port it to some branch for an earlier release
version. Suppose that you've made your commit for the `main` PR and it has commit ID `abcd1234`.
The following stpes will pull over your commit to the other branch so you can test it there and
create a PR for the branch as well.

```
# Create a branch for your work off of the release/1.23.x branch
git co -b issue-1234-1.23.x origin/release/1.23.x

# Pull over your commit into your current branch
git cherry-pick abc1234

# (Optional) edit the commit message if you want
git commit --amend

# Push your changes to your fork. Be sure to specify both branch names!
git push fork issue-1234-1.23.x:issue-1234-1.23.x
```

You can then push your new branch up and set up a PR against the `release/1.23.x` branch.

## Reverting vs Resetting
**NOTE: Using git-reset will rewrite history. Be VERY CAREFUL using this command. NEVER REWRITE PUBLIC HISTORY**

Reverting creates a sort of "anti-commit". That is, if commit `A` changes `x` to `y`, then
reverting commit `A` creates a new commit `B` where `y` is changed to `x`. This does NOT rewrite
history, so it is the safer option if you've already merged commits into publicly available
history.

Reset can be used to (among other things) fully delete commits. This has the potential to be
**super dangerous**. If you have not yet pushed your commits to a remote repository, there is less
to fear. But if you delete a commit that only exists locally, you cannot get it back, so still be
careful, especially with the `--hard` option.

| Command | Description |
|---|---|
| `git revert <commit-id>` | Creates a commit that undoes the specified commit ID. |
| `git reset --soft HEAD~1` | Unwind commits up to the first one after the current HEAD, but leave the file changes it contained staged (undoes ONE commit) |
| `git reset --hard HEAD~3` | Unwind commits up to the third one after the current HEAD and fully remove the changes from the working copy (undoes THREE commits) |

## How to Revert a Faulty Merge
If you need to revert a merge commit (say a PR was merged, but you need to "undo" it for some
reason), things can get complicated. For a very thorough discussion of the relevant factors
and steps, see [this post](https://github.com/git/git/blob/master/Documentation/howto/revert-a-faulty-merge.txt).

## How to Rebase
I'll fill this section in later... it is more complicated to describe
**NOTE: Using git-rebase involves rewriting history, so take great caution with this command. NEVER REWRITE PUBLIC HISTORY**
