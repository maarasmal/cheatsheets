# CentOS-Specific Command help

## Check on the status of a service (root)
`systemctl status sshd`

## Listing systemd services
```
systemctl list-units --type=service                    # List all services
systemctl list-units --type=service --state=running    # List only running services

systemctl list-unit-files --state=enabled      # List which services start at bootup automatically

systemctl status foo.service          # Get the status of a service named by the previous commands
```


## Various journalctl commands
These should all be fairly self-evident as to what they do. Running `journalctl` requires root
access (or sudo).

```
journalctl --list-boots
journalctl -b               # current boot
journalctl -b -1            # previous boot

journalctl --since "2020-01-07 12:17:09"
journalctl --since "1 hour ago"
journalctl --since yesterday
journalctl --since yesterday --until "1 hour ago"

journalctl -u docker.service   # the .service part is usually optional
journalctl -u svc1 -u svc2 --since yesterday  # merges messages from 2 services
journalctl _PID=12345

```

## Force log rotation
Typically, the config file is at `/etc/logrotate.conf`.
```
logrotate -vdf /path/to/logrotate.conf  # for a dry run; this won't actually do rotations

logrotate -vf /path/to/logrotate.conf
```

