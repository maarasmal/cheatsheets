# Vault

[Full CLI Docs](https://www.vaultproject.io/docs/commands)

*IMPORTANT*: Secrets can have more than one key:value pair. If you `vault write` a secret and
specify just a single key:value pair, then all of the other fields will be deleted from the
secret. If using [K/V Version 2](https://www.vaultproject.io/docs/secrets/kv/kv-v2), then you
can use `vault kv patch ...` to update just a single field of a multi-field secret without
destroying the other values.

## Listing Secrets

To list the secrets under a given path:
```
vault list secret/myorg/myproj
```


## Writing secrets

Write a secret with a single field named `value`
```
vault write "secret/myorg/myproj/mysecret" value=password123
```

NOTE: the command above will leave the secret value in both your terminal scrollback and your
shell command history. A little further down, there is an example showing how to write a keystore
(binary data) to a secret by telling `vault write` to read the value from a file. You can do
something similar by first writing your non-binary secret to a text file (do not do something like
`echo password123 > mysecret.txt`, since that has the same issue as the above command), then using
that file as the input to `vault write`. Assuming you have your secret value already stored in a
file `secretvalue.txt`, you can do this:

```
vault write "secret/myorg/myproj/mysecret" value=@secretvalue.txt
```

ANOTHER NOTE: Be wary of trailing CRLF characters when creating a text file with your secret in
it. The _entire_ file will be read and stored as the secret in Vault, including that trailing
CRLF, which will likely cause issues later on when the secret value is read and used. Note that
many editors will silently add a CRLF at the end of your file when you save the file if the file
doesn't already have one. You can use any technique you like to avoid a trailing CRLF, just make
sure it isn't included.

On a Mac, one simple trick to work around this is to copy the value from some other source by
highlighting it with your mouse so that you select precisely only the proper value for the secret,
then using Ctrl+C (or similar) to copy it to the pasteboard, and then you can write the value to a
file with a command like:

```
echo $(pbpaste) > secretvalue.txt
```

You can do something like `ls -l` to verify the size of the file in bytes matches the expected
size. If you have a 10-character password that you wrote to a file, the file should be 10 bytes,
not 11.

Store a keystore file (or any binary secret data) in a secret by first encoding it in base64:
```
cat my-keystore.p12 | base64 > my-keystore.b64
vault write "secret/myorg/myproj/mykeystore" value=@my-keystore.b64
```

## Reading secrets

Print all of the key/value pairs stored under a secret:
```
vault read secret/myorg/myproj/mysecret
```

Read a single field named `value` from a secret (useful in scripts):
```
vault read -field=value "secret/myorg/myproj/mysecret"
SECRET_PASSWD=$(vault read -field=value secret/myorg/myproj/my-keystore-pw)
```


Read a base64-encoded keystore file out of Vault and decode it:
```
vault read -field=value "secret/myorg/myproj/mykeystore" | base64 --decode --output my-keystore.p12
```

## Deleting secrets

```
vault delete "secret/myorg/myproj/mysecret"
```


