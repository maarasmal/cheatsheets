# Useful commands for firewalld and firewall-cmd on CentOS 7

The official `firewall-cmd` reference material is [here](https://firewalld.org/documentation/man-pages/firewall-cmd.html)

| Command | Description |
|---|---|
| `firewall-cmd --set-log-denied=all` | Log denied packets. Other values like unicast, broadcast are available. `off` disables logging. Messages are written to `/var/log/messages`, unsure if they are viewable via `journalctl` |
| `firewall-cmd --get-log-denied` | Print the current value of the `log-denied` setting |
