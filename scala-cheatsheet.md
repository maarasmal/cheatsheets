# Scala tips and tricks
These mostly center around configuration items, not really coding items (see the sbt-sandbox
project for many Scala coding examples).

# What is that thing called?
A handy reference for operators/symbols/syntax:
https://github.com/andyscott/scala-whats-that-called

# Performance Characteristics
[Scala 2 Collections](https://docs.scala-lang.org/overviews/collections-2.13/performance-characteristics.html)


## Operator associativity
The associativity of an operator is determined by the operator's *last* character. Operators that
end in `:` are right-associative. All others are left-associative.

Thus, `foo ^^ bar` desugars to `foo.^^(bar)`, while `foo ^^: bar` desugars to `bar.^^:(foo)`

## Variances

```
class Foo[+A]  // A covariant class
class Bar[-A]  // A contravariant class
class Bam[A]   // An invariant class
```

Covariance: If `A` is a subtype of `B`, then `Foo[A]` is a subtype of `Foo[B]`.

Contravariance: If `A` is a subtype of `B`, then `Bar[B]` is a subtype of `Bar[A]`

Invariance: If `A` is a subtype of `B`, there is no type relationship between `Bam[A]` and `Bam[B]`.

More details on variances: https://docs.scala-lang.org/tour/variances.html

## Type Bounds
Type parameters can be given upper/lower type bounds.

Upper type bound example:

```
def foo[T <: A](t: T): String = ???
```

This means that `T` must be a subtype of `A`.

Lower type bound example:

```
trait Node[+B] {
  def construct[U >: B](elem: U): Node[U]
}
```

This means that `U` must be a supertype of `B`. It would be needed for `construct` because the `B`
type parameter to `Node` is covariant. Functions, by default, are _contra_variant in their
parameter types and _co_variant in their result types. If we tried to make `elem` be of type `B`,
that would conflict because `B` is covariant.

## Disable annoying log messages from Akka during testing
For AP, Akka would log a bunch of annoying messages about actors being terminated abruptly during
tests, which is expected. These messages were not affected by the `logback.xml` configuration.
This configuration went into `src/test/resources/application.conf` to tell Akka directly to not
log those so-called "errors"

```
akka {
  loggers: ["akka.event.slf4j.Slf4jLogger"]
  loglevel: INFO
  log-dead-letters: off
  log-dead-letters-during-shutdown: off
}
```

## Simple Ordering definitions for ADTs
Given a simple case class like so:
```
final case class Foo(id: Foo.Id,
                     name: Name,
                     importDate: java.time.Instant)
```

Assuming that there is an `Ordering[T]` for each of the fields that you want to order by, you can
define an `Ordering[Foo]` like so:

```
implicit val ordering: Ordering[Foo] = Ordering.by[Foo, (Name, Instant, Foo.Id)] { foo =>
  (foo.name, foo.importDate, foo.id)
}
```

Much better than writing a bunch of nested `if` statements

# Partial functions instead of explicit match/case statements
This is perfectly legal code:

```
listOfInts.map { value =>
  value match {
     case 1 => "one"
     case 2 => "two"
     case _ => "I can't count that high"
  }
}
```

But it is possible to simplify this a bit:

```
listOfInts.map {
   case 1 => "one"
   case 2 => "two"
   case _ => "I can't count that high"
}
```

Functions like `map`, `flatMap`, etc. take a `Function1`. The second case above works because
`PartialFunction` extends `Function1`.

# Debugging of Cats/cats-effect issues

Error something like:

```
value flatMap is not a member of type parameter F[foo.bar.Thing]
```

Pilquist suggested the following approach:

> My general approach to debugging that kind of error:
> 1) What type class provides the function in question? `FlatMap[F]` provides `flatMap` function I need
> 2) Do I have the syntax for that type class imported? `cats.implicits._` for syntax for cats-core typeclasses, `cats.effect.implicits._` for cats-effect type classes
> 3) Do I have an instance of the type class or an instance of a subtype of the type class? In this case, I have `Sync[F]` which is a subtype of `FlatMap[F]`
> 4) Do I have multiple conflicting instances of subtype of `FlatMap[F]`? Not in this case

# Scheduling tasks
Suppose you have a task that you want to run once at startup, and then periodically after that:

```
def doPeriodicTask[T](): IO[T] = ???
val periodicInterval: Duration = ???

doPeriodicTask().unsafeRunSync
Stream.awakeEvery[IO](periodicInterval).evalMap { _ =>
  doPeriodicTask()
}
```

Calling `unsafeRunSync` yourself is Not Good. The better way to do that is:

```
def doPeriodicTask[T](): IO[T] = ???
val periodicInterval: Duration = ???

Stream.eval_(doPeriodicTask()) ++ Stream.awakeEvery[IO](periodicInterval).evalMap{ _ =>
  doPeriodicTask()
}
```

# ScalaCheck shrinking issues

This gist was helpful in figuring out how to stop shrinking when you don't want it:
https://gist.github.com/davidallsopp/f65d73fea8b5e5165fc3

As of this writing, my most recent issue was with a `Gen.oneOf()`, which I resolved via:

```
val values: List[String] = ???
forAll(Gen.oneOf(values).suchThat(values.contains)) { ...
```

I had wanted one of a defined set of strings, such as "abc", "def", but it was choosing a value
and then when an error occurred, it was shrinking "abc" to "ab" and then "a" and then "", but the
shrunken values were meaningless and invalid in the context where I was trying to use them.

## Stupid conversions

Convert a base 64 string to text:
```
import java.util.Base64
val decoder = Base64.getDecoder
val base = ??? // some base-64 encoded string
decoder.decode(base).map(_.toChar).mkString
```
Decode a base 64 encoded byte array:
```
val base = ??? // some base-64 encoded string
import scodec.bits._
val decoded = ByteVector.fromBase64(base)
```


