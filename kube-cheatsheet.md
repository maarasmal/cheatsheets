# Kubernetes help
Some notes mostly based on Getting Started with Kubernetes, a training video on Pluralsight by
Nigel Poulton.

## Installing Kubernetes on Mac
`brew install kubectl`
`brew cask install minikube`
<optionally install optional VM driver support, xhyve, virtualBox, etc.>

## Minikube commands
These instructions were pulled from the training video, and are likely a little out of date in
some areas. But they should serve as a decent jumping off point to get started.
| Command | Description |
|---|---|
| `minikube start` | Start the minikube cluster |
| `minikube start --kubernetes-version="v1.6.0"` | Start the minikube cluster, forcing the K8S version to 1.6.0 |
| `minikube stop` | Stop the cluster, saving its state |
| `minikube delete` | Delete the cluster, allowing you to start over if need be |
| `minikube dashboard` | Launch your browser w/ a GUI for inspecting your cluster |

## Basic kubectl stuff
| Command | Description |
|---|---|
| `kubectl config current-context` | List the cluster that kubectl is currently "talking to". You can swap the context to talk to a different cluster |
| `kubectl get nodes` | List the nodes in the cluster |
