# GDB tricks

I found a pretty concise [cheatsheet](https://darkdust.net/files/GDB%20Cheat%20Sheet.pdf) PDF
online. Refer to that first before adding content here.

## GDB configuration

I used the following as my `~/.gdbinit` for a project at Arris:

```
#---[ GDB configuration ]-----------------------

set print array on
set print pretty on

# The default output-radix is 10, can be 8 or 16 as well
# set output-radix 16

set history save on
set history size 256


#---[ Breakpoint aliases ]----------------------

define bpl
    info breakpoints
end
document bpl
List all breakpoints
end
```
