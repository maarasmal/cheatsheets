# Recording Training Sessions for Dummies

Author: Dummy Lamarre

These are just some notes that capture some of the setup and lessons learned when we recently had
to scramble to figure out how to record some upcoming training sessions. There are probably a lot
of flaws and opportunities to improve these procedures, but this is what we ended up with. It is
not perfect, but it will suffice. Recall that we are software engineers, not audio/visual experts.

NOTE: The biggest issue with these procedures is that the microphone of the device doing the
recording is not included in the recorded audio. So if this is an interactive session that you are
recording, your own voice will not be present. I am sure there is a way to rectify this, but fixing
that issue was beyond the scope and available time of the current situation.

Other Note: This worked for this author on a MacBook Pro that was running Catalina 10.15.7 (yes,
in August of 2022, still Catalina). Your mileage may vary on newer versions of macOS.

## Setting Up Beforehand

[Recording your screen on macOS](https://support.apple.com/en-us/HT208721) is easy. But getting the
audio from your Teams call recorded along with your screen is a little more of a challenge.

### BlackHole Installation
You will want to download and install [BlackHole](https://existential.audio/blackhole/), an open
source virtual audio driver. You will need to give them a name and email before they will email
you a link to download the driver. They offer 2ch, 16ch and 64ch versions. I opted for the 16ch
version.

Installation instructions for BlackHole can be found on
[their GitHub page](https://github.com/ExistentialAudio/BlackHole#installation-instructions). A
summary of those instructions is included in the information below.

### Virtual Device Setup

1. Open the `Audio MIDI Setup` app on your Mac
1. Click the `+` button at the bottom left of the device list and select
   `Create Multi-Output Device`
1. Select the new `Multi-Ouput Device` in the list on the left of the window
1. Select the checkbox in the `Use` column for the devices you wish to include. Typically this would
   include `Built-in Output` (also sometimes called `MacBook Pro Speakers`) and `Blackhole 16ch`
   (or whichever version you opted for)
   * The `Master Device` should be set to `Built-in Output` (or `MacBook Pro Speakers`)
   * Note that `Built-in Output` (or `MacBook Pro Speakers`) should be listed first. If it is not,
     de-select any other devices that you had enabled, which should float it to the top of the list.
     Then you can re-select the other devices you wish to include in this multi-output device.
   * For each device _other than the `Master Device`_, select the checkbox in the `Drift Correction`
     column.
1. In the list on the left of the window, right click on your `Multi-Output Device` and select
   `Use This Device For Sound Output`.
   * If you have the Sound control enabled in your system menu bar, you can switch between this
     device and others without having to launch `Audio MIDI Setup` in the future. If the Sound
     control is not displayed in your system menu bar, you can enable it via the Sound panel in
     `System Preferences`.
1. You can quit `Audio MIDI Setup` now.

## Recording Your Teams Call

The steps above should be done well ahead of time. Once your call is imminent, you can perform the
following steps to record your session.

## Teams Setup

1. Launch Teams and open the preferences with `Cmd` + `,`
1. In the list on the left of the window, select `Devices`
1. In the Audio Devices section, under the label Speaker, select your new
   `Multi-Output Device (Aggregate)`
1. Close the Settings window and join your Teams meeting
1. Configure your screen layout, maximize your window, etc.

## Start the Recording
[Reference](https://support.apple.com/en-us/HT208721)

1. Press `Cmd` + `Shift` + `5`. The recording controls will appear. Note that if you have a
   multi-monitor setup, the controls will probably appear on your primary monitor, not the monitor
   that had focus when you pressed that keyboard shortcut.
1. Select `Record Entire Screen` or `Record Selected Portion`, as desired.
1. Click on the `Options` menu. In the `Microphones` section of the menu, select `BlackHole 16ch`.
   Then select any other desired options from the menu.
1. Click the `Record` button to start recording immediately.
   * Note that while your _voice_ will not be included in the recording, your _on-screen actions_
     will be. This includes your mouse movements, any notifications that pop on the screen. The
     recording is bound to your _display_, not to a particular desktop, so if you make your Teams
     window full screen, start recording, then switch to another virtual desktop on that display,
     the recording will capture the switch to the other desktop, etc.
1. To stop recording, click the `Stop Recording` button that will appear in your system menu once
   recording has started.

The resulting file will be saved to whichever location you selected from the `Options` menu. By
default, the recording will be on your `Desktop`.

## Fixing It In Post

Navigate to your recording. You can open and play it via the `QuickTime Player` app on your Mac.

Notice that your file is probably quite large. As of this writing, a recording a little over 10
minutes long clocked in at nearly 500 MB. You are almost certainly going to want to transcode that
file into something a little more svelte.

### HandBrake

1. Download and install [HandBrake](https://handbrake.fr)
1. Launch the `HandBrake` app. It should immediately ask you to select a file to open. Navigate to
   your screen recording `.mov` file and open it.
1. `HandBrake` will scan your movie and then show you some data about the file and some preset
   transcoding options. The `Preset` menu will probably have `Fast 1080p30` selected. This is a
   reasonable default, but you can select another option from the `Preset` menu if you wish. For
   the rest of this example, we will assume that you start with `Fast 1080p30` selected, and then
   we will make a couple modifications.
1. On the `Summary` tab, make sure that the Format is set to `MP4` and that `Web Optimized` is
   selected.
1. On the `Video` tab, select `60` from the `Framerate (FPS)` menu.
1. At the bottom of the window, adjust the output file name and output location as desired.
1. At the top of the window, click the Start button
   * Note that the transcoding process will take a decent bit of time, and longer videos will take
     longer to convert. On this author's Intel MacBook Pro, the CPU usage of `HandBrake` is
     significant enough to make other UI actions a little sluggish during the transcode. Perhaps
     the M1 MBP's will chug through transcodes a little more efficiently.

This is anecdotal, but the steps above reduced a ~10.5 minute test recording from a 486 MB .mov
file down to a 27 MB .mp4 file with little visible loss in video quality.


