# Docker Cheatsheet
For details, full documentation for the `docker` command is [here](https://docs.docker.com/engine/reference/commandline/cli/)

Documentation for `docker-compose`, including a config file reference is [here](https://docs.docker.com/compose/overview/)

A great, clear tutorial on installing `colima` (an open-source container runtime alternative) can
be found [here](https://smallsharpsoftwaretools.com/tutorials/use-colima-to-run-docker-containers-on-macos/) 

## docker commands
To log in to a registry, you might want to add the following in `/etc/docker/daemon.json`, at least for initial testing:

```
{
  "insecure-registries" : [ "my-repo-server:5000" ]
}
```

To see the servers for which you have already done `docker login ...` (this isn't a true login
session), you can view the file `~/.docker/config.json`. Specifically, the `auths` section.

| Command | Description |
|---|---|
| `docker login https://my-repo-server:5000` | Log the daemon in to a remote Docker registry |
| `docker run -t my-repo-server:5000/foo/bar:2.0.1 java -version` | Run the command `java -version` in the named Docker image |
| `docker run -it --entrypoint bash image_name` | Run command `bash` in a container based on image `image_name`, but override any potential ENTRYPOINT definition for the image. **NOTE:** this command assumes your image has `bash` included in it. See the general notes below all of the tables if it does not. |
| | This is useful when you just want to browse around the innards of a container without actually running whatever its default entrypoint is. Without the `--entrypoint`, the default entry point for the container would fire as well as your `bash` process |
| `docker run -it --rm --entrypoint bash image_id` | Same as above, but automatically remove the container when it exits |
| `docker ps` | List the running containers |
| `docker ps -a` | List all containers, including those that are stopped |
| `docker images` | List all loaded images |
| `docker rm <container>` | Removes a running container. The name of the container can be either the container ID or the IMAGE name (see the output of `docker ps`) |
| `docker rm $(docker ps -a -q)` | Remove all stopped containers |
| `docker rmi <image>` | Removes an image from the local repository. `<image>` can be a REPOSITORY, REPOSITORY/TAG, or an IMAGE ID (see the output of `docker images`) |
| `docker pull my-repo-server:5000/foo/bar:1.0.2-SNAPSHOT` | Download an image from a remote repository (does not start it) |
| `docker save -o imgname.tar foo/bar:1.0.3-SNAPSHOT` | Export a full Docker image to `imgname.tar`. This image can be imported later via `docker load`. |
| `docker save foo/bar:2.0.0-SNAPSHOT > imgname.tar` | Same as the previous command |
| `docker load --input imgname.tar` | Load the specified image file into the local registry |
| `docker load < imgname.tar` | Load the specified image file into the local registry |
| `docker history someimg` | Displays a sort of list of commands that show what modifications have been done to create the layers of `someimg` |
| `docker port cname` | List the port mappings for the container named `cname` |
| `docker cp <container-id>:/etc/hosts .` | Extract the `/etc/hosts` file from within the container identified by `<container-id>` |
| `docker tag e7ee83937207 myorg/myapp:1.2.3` | Manually tag image ID `e7ee83937207` with repository `myorg/myapp` and tag `1.2.3` |
| `docker logs <container-id>` | View logs for the specified container. Typically configured via options in `docker-compose.yml`. For Docer CE, only works when logging driver is `local`, `json-file` or `journald`. |
| `docker system prune` | Remove unused data (e.g., stopped containers, dangling images, volumes without containers, etc.). More targeted pruning commands are shown below. |

## docker-image commands
| Command | Description |
|---|---|
| `docker image inspect someimg` | Prints out the configuration and layers of the image `someimg` |
| `docker image inspect foo/bar:1.0.1` | Prints information about the specified image. Useful in scripting because it returns a proper status if the image is or is not found |
| `docker image tag someimg myreg/myrepo/someimg:tagname` | Create a tag for the local image `someimg` that refers to the registry `myreg`, the repository in that registry named `myrepo`, with an image name of `someimg` and a tag of `tagname` |
| `docker image push myreg/myrepo/someimg:tagname` | Upload the specified image up to the named repository. You must be logged in to do this (i.e., `docker login myreg`), and you must have permissions to upload to that repository |
| `docker image prune` | Deletes dangling images. Useful after repeated runs of `dockerComposeUp` and the like. |

## docker-container commands
| Command | Description |
|---|---|
| `docker container run -it alpine sh` | Run the `alpine` container, interactively, and start a shell process within it. Disconnect from the interactive session with `Ctrl`+`P`+`Q`. **NOTE:** this leaves the shell process running in the container. You can use `exit`, of course, but if the container has no active processes, it will be stopped automatically. |
| | Note as well that in this case, `sh` replaces the default command that the container is configured to run. Use `docker image inspect` and look for the `Cmd` entry to see what the default command is for the image. In the case of Alpine, the default is `sh`, so leaving off the `sh` from the `docker container run...` command above results in the same behavior. |
| | If the container is configured with an ENTRYPOINT instead of a CMD, run-time arguments are instead appended to ENTRYPOINT, instead of overriding it, as is the case with CMD. |
| `docker container stop <name-or-id>` | (`id` is printed when you started the container w/ `docker container run ...`) Stops the container. The container is still loaded and visible with `docker container ls -a`. The state of the container is persisted. |
| `docker container start <name-or-id>` | Starts a stopped container. Previous state data inside the container is restored. |
| `docker container exec -it <name-or-id> sh` | Runs `sh` inside an existing container as identified, and attaches to it interactively |
| `docker container exec <name-or-id> ls -l` | Runs `ls -l` inside the container and prints the output to our terminal, returning control to the host shell where you entered the command. Useful for one-off commands when you do not want to fire up a shell and do things inside the container. |
| `docker container rm $(docker container ls -aq) -f` | Destroy all containers, even those that are not running |
| `docker container prune` | Remove all stopped containers |


## docker-swarm commands
This allows you to configure Docker services running on multiple hosts to act as a cluster. All
distributed consensus processing is handled by Raft.

The ideal number of managers is 3, 5 or 7.

For many of these commands, it is advisable to add
`--advertise-addr <ip:2377> --listen-addr <ip:2377>`, where the IP address is **the address of the
node you are on and trying to add to the swarm**, etc. As a shorthand, we will note these
additional recommended parameters in the examples below as `<ADDR-OPTS>`

| Command | Description |
|---|---|
| `docker swarm init <ADDR-OPTS>` | Start a swarm with the current node as a manager |
| `docker swarm join-token manager` | Prints out instructions for how to add another manager to this swarm |
| `docker swarm join-token worker` | Prints out instructions for how to add another worker to this swarm |
| `docker swarm join --token <token> <manager-ip:2377> <ADDR-OPTS>` | Join the swarm identified by the token. The value of the token determines whether you will join as a manager or a worker. The value for `<token>` can be obtained via the `join-token` commands shown above |
| `docker node ls` | Print information about the swarm. Current node you are on is indicated with an `*` after the `ID` value. **Can only be run from a manager node.** |
| `docker node promote <ID>` | Promote the specified node from worker to manager. Must be run from another manager (maybe it has to be from the leader?) |

## docker-service commands
Where `docker-swarm` configures the hosts in a cluster, `docker-service` configures the containers
that will run across that swarm. So if you define a 3-node cluster, then a service with 6 replicas
of the image, you should end up with 2 containers on each node of the swarm.

| Command | Description |
|---|---|
| `docker service create --name myservice -p 8080:8080 --replicas 5 foo/bar-image` | Create a service named `myservice`, with all nodes mapping port `8080` from the host to `8080` on the container, with 5 replicas of the image running in the swarm, using the image `foo/bar-image`. |
| `docker service ls` | List informaiton about running services |
| `docker service ps myservice` | Prints information about all of the images running across all nodes that make up the service `myservice` |
| `docker service inspect myservice` | Prints detailed configuration information about the service `myservice`, including networking |
| `docker service inspect myservice --pretty` | Same as above, but nicer formatting |
| `docker service scale myservice=7` | Update the number of replicas desired for `myservice` to be 7. New containers will be automatically spun up across the swarm. |
| `docker service update --replicas 7 myservice` | Same as the previous command |
| `docker node ps <NODE>` | Displays the tasks running on this node. `<NODE>` is a value displayed by the `docker service ps` command above. |

## docker-stack commands
Stacks build upon services and swarms. Stacks are defined in YAML files, very similar to
`docker-compose.yml` files, just with multiple services, networks and volumes defined. Also, they
must start with `version: 3.x` or higher.

Stacks basically replace the old `docker-compose` tool, which is not needed at all for using stacks.

| Command | Description |
|---|---|
| `docker stack deploy -c stackfile.yml` | Deploy the entire stack defined within `stackfile.yml`. Creates all of the networks and services defined in the configuration file, across all of the nodes of the swarm. |
| | The same command can be used to UPDATE the stack if you make changes to the stack definition file. The running stack will be modified (services/networks/volumes added/dropped as necessary) to match the new stack definition. |
| `docker stack ls` | List the stacks currently active on this host |
| `docker stack ps <stackname>` | List the state of the containers that comprise this stack. `<stackname>` is printed by `docker stack ls` |
| `docker stack services <stackname>` | Displays the state of the services defined that make up the stack `<stackname>` |

## docker-compose commands

| Command | Description |
|---|---|
| `docker-compose up -d` | Start a docker container via a `docker-compose.yml` file in the current directory |
| `docker-compose down` | Stop a docker container via a `docker-compose.yml` file in the current directory |
| `docker-compose restart` | Restart a container via a `docker-compose.yml` file in the current directory. *NOTE:* This does *NOT* pick up changes you made to the `docker-compose.yml` file. |
| `docker-compose up -d --force-recreate -V --remove-orphans` | Fully reload the container(s). This *DOES* pick up changes to `docker-compose.yml`. |

---

## General Docker things

### Inspect the contents of an image without really running it
I cannot summarize it any better than this SO answer did: https://stackoverflow.com/a/46526598. As
of when I wrote this note, the accepted answer on that question is fine if your image contains the
`sh` executable, but as the linked answer points out, that is not always going to be the case. The
solution is a bit of a hack, but does achieve the desired goal.

### Docker daemon/engine logs
Logs on a systemd-based system (such as CentOS) can be viewed with `journalctl -u docker.service`.
For other Linux systems, probably just check `/var/log/messages`.

### Container/app logs
Apparently the standard is for PID 1 to write its logs to stdout and stderr. If your app writes to
a file, you can use symlinks to shunt that data over to stdout/stderr. Docker will then interface
with logging drivers like syslog, gelf, splunk, fluentd to persist your app logs through those
services. The default logging driver is configured in `daemon.json`. (That can be overridden per
container at runtime.)

By default most docker hosts default to JSON file logging. You can then use the
`docker logs <container-name>` command to view the logs.

Alternatively, you can mount a volume to the containers log file locations so that you can persist
the log files outside of the container after it is stopped. (This is what we have done at CCAD/Arris)

### SSH Agent Forwarding
I had a case where I wanted to share my `~/.ssh` directory into a container so that it could use
my identity to run Ansible commands. To avoid getting prompted for my key password when Ansible
was run inside the container, I also wanted to forward my `ssh-agent` into the container, since
that will typically already have my identity loaded. Documentation for how to do that can be
found here: https://docs.docker.com/docker-for-mac/osxfs/#ssh-agent-forwarding

The short version is that you need to add the following lines to your `docker run` command where
you are starting the container:

```
--mount type=bind,src=/run/host-services/ssh-auth.sock,target=/run/host-services/ssh-auth.sock
-e SSH_AUTH_SOCK="/run/host-services/ssh-auth.sock"
```

And of course, you need to mount your `~/.ssh` directory into the container by adding this option
as well:

```
-v $HOME/.ssh:/root/.ssh
```

Lastly, since I was running a Linux container on a Mac host, the container's OpenSSH did not like
the `UseKeychain` entry that I had in my `~/.ssh/config` file. This issue is discussed here:
https://developer.apple.com/library/archive/technotes/tn2449/_index.html

