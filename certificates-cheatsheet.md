# Lessons Learned Through Much Pain and Tears
Why are certificate-related error messages so unhelpful?

[DER vs. CRT vs. CER vs. PEM Certificates and How To Convert Them](https://support.ssl.com/Knowledgebase/Article/View/19/0/der-vs-crt-vs-cer-vs-pem-certificates-and-how-to-convert-them)

## OpenSSL Commands

### Print the basic info for a certificate file
`openssl x509 -in somecert.cer -noout -text`

### Print the basic info for a DER-formatted certificate file
`openssl x509 -in somecert.cer -inform DER -noout -text`

### Convert PEM to DER
`openssl x509 -in cert.crt -outform der -out cert.der`

### Convert DER to PEM
`openssl x509 -in cert.crt -inform der -outform pem -out cert.pem`

### Print out the basic info for a CSR
`openssl req -noout -text -in myrequest.csr`

### Verify a certificate chain
Usually you will get the root and intermediate certs back with your signed cert.
```
openssl verify -x509_strict -purpose sslclient -CAfile chain.pem mycert.crt
```
The `chain.pem` file should contain the certificate chain for everything other than your cert (that
is, the root and all intermediates). There are some potential security issues with various flags
related to the `verify` command, so be aware of those. Some more information is
[here](https://stackoverflow.com/questions/25482199/verify-a-certificate-chain-using-openssl-verify)

I also managed to verify a chain with a command like the following:
```
openssl verify -CAfile root.pem -untrusted subca.pem server.cer
```

### List the certs in a PKCS12 keystore/truststore
`openssl pkcs12 -nokeys -info -in /path/to/file.p12 -passin pass:<password>`
You can omit the `-passin` parameter and be prompted for the password instead.

### Extract the private key from a PKCS12 keystore
```
openssl pkcs12 -in my-keystore.p12 -out keys-out.txt
```

You will be prompted for the import password. This is the password for the keystore, established
when it was first created.

You will then be asked for the PEM passphrase. Enter a new passphrase for the private key that you
are exporting. Enter it a second time when prompted to verify the same value.

This will dump the certificate and **encrypted** private key to `keys-out.txt`. You can copy/paste
the key to a separate file (`enc-private-key.txt`) to decrypt it with the following command:

```
openssl rsa -in enc-private-key.txt -out private.key
```

When prompted, enter the PEM passphrase that you entered when generating `keys-out.txt` above.

### Extract the private key from a PKCS12 keystore (without the BS)
The above method is a little more secure, since the key is encrypted before export. But if you
know what you're doing and you just want the key, this command will get it without the encrypting
and it will also be in PKCS8 format instead of RSA (omit `-nodes` for PKCS#1 format):

```
openssl pkcs12 -in my-keystore.p12 -nocerts -nodes -out private.key
```

reference: https://stackoverflow.com/a/9516936

### Import signed (renewed) cert into existing keystore

```
openssl pkcs12 -in NamedCert.pem -inkey PK_key.pem -certfile INTERMEDIATECERT.pem \
  -export -out NAMEDCERT-keystore.pkcs12 
```

### Connect to a server to see the certificate details
Useful after deploying a new cert to a server
```
openssl s_client -connect host:port
```

---

## Keytool commands

### Create a new (empty) JKS keystore
This will create a default key named `mykey`, which can then be deleted:
```
keytool -genkey -keyalg RSA -keystore keystore.jks -keysize 2048
keytool -delete -keystore keystore.jks -alias mykey
```

### Print information for all certificates in a keystore/truststore
There are other options depending on your store type, etc., but the most basic form is:
```
keytool -list -v -keystore mykeystore.jks -storepass changeit
keytool -list -v -keystore mytruststore.jks -storepass changeit
```
To see info for just one certificate:
```
keytool -list -v -alias myalias -keystore mytruststore.jks -storepass changeit
```

### Create a new self-signed certificate in its own keystore:
```
keytool -genkey -keyalg RSA -alias my-test-cert -keystore my-test-keystore.jks \
  -storepass changeit -validity 360 -keysize 2048
```
Same, but set the full distinguished name so as to not get prompted:
```
keytool -genkey -keyalg RSA -alias my-test-cert -keystore my-test-keystore.jks \
  -dname "CN=my.fqdn.com, OU=myOrgUnit, O=My Corporation, L=San Diego, S=CA, C=US" \
  -storepass changeit -validity 360 -keysize 2048
```

### Create a CSR for that new certificate:
```
keytool -certreq -alias my-test-cert -keystore my-test-keystore.jks \
  -file my-test-cert.csr
```

### Import a root or intermediate CA cert into the keystore:
```
keytool -import -trustcacerts -alias root -file SomeRootCA.crt \
  -keystore my-test-keystore.jks
```

### Import the signed certificate generated from the CSR above:
```
keytool -import -trustcacerts -alias my-test-cert -file my-test-cert.crt \
  -keystore my-test-keystore.jks
```

### Import signed (renewed) certificate into an existing keystore
```
keytool -importcert -storetype PKCS12 -keystore $APPLICATION_NAME-truststore.pkcs12 \
  -storepass $TRUSTSTORE_PASSWORD -alias ca -file ca_cert.pem -noprompt
```

### Export a certificate from the keystore:
```
keytool -export -alias my-test-cert -file my-test-cert.crt -keystore my-test-keystore.jks
keytool -exportcert -keystore ./path/to/truststore.p12 -storetype PKCS12 -alias somecert -file somecert.crt
```

### Export an entire chain from the keystore (in this case, the keystore is in JCEKS format):
```
keytool -list -alias my-test-cert -keystore my-test-keystore.jceks -storetype JCEKS -rfc
```

### Change the name of an alias in the keystore
```
keytool -changealias -keystore my-test-keystore.jks -alias my-test-cert \
  -destalias new-test-alias
```

### Delete an entry from the keystore
```
keytool -delete -keystore my-test-keystore.jks -alias my-test-cert
```

### Import a root or intermediate certificate into the keystore
The same command is used for both root and intermediate certs
```
keytool -import -trustcacerts -alias some-root-ca -file SomeRootCA.crt -keystore my-test-keystore.jks
```

### Change the password of a keystore/truststore
```
keytool -storepasswd -keystore my.keystore
```
This will prompt you to enter a new password for the keystore `my.keystore`.

### Change the password for a specific key in a keystore
```
keytool -keypasswd  -alias <key_name> -keystore my.keystore
```
This will prompt you to enter a new password for the key with the alias `<key_name>`.

---

# General Certificate Notes, Best Practices, etc.

- Only ever use the root certs from your truststore, never one that was just given to you with the
  request, etc.
- When storing a cert to the keystore, typically the entire chain (minus the root) is saved in the
  keystore
- For mutual authentication, your truststore needs not only the root (and possibly the
  intermediates) of your own cert, but at least the root of the remote server you with whom you
  are authenticating
- One reason you would choose to store the entire trust chain in your trust store is so that at app
  startup, you can kind of do a self-authentication of your certificate against the chain


---

# Unsorted stuff
Mike P. mentioned these sample commands in Slack, but I haven't had time to categorize them yet:

```
keytool -import -alias root -file certs/ca.cert.pem -keystore out/truststore-multilevel-root.jks -noprompt -storepass changeit
keytool -import -alias root -file certs/ca.cert.pem -keystore out/truststore-multilevel-full.jks -noprompt -storepass changeit
keytool -import -alias intermediate -file intermediate/certs/intermediate.cert.pem -keystore out/truststore-multilevel-full.jks -noprompt -storepass changeit
openssl pkcs12 -export -passin pass:changeit -in intermediate/certs/server.cert.pem -inkey intermediate/private/server.key.pem -passout pass:changeit -out server.p12 -name server -CAfile intermediate/certs/ca-chain.cert.pem -caname root
keytool -importkeystore -deststorepass changeit -destkeypass changeit -destkeystore out/server-multilevel.jks -srckeystore server.p12 -srcstoretype PKCS12 -srcstorepass changeit -alias server
openssl pkcs12 -export -passin pass:changeit -in intermediate/certs/server.cert.pem -inkey intermediate/private/server.key.pem -passout pass:changeit -out server-full.p12 -name server -CAfile intermediate/certs/ca-chain.cert.pem -caname root -chain
keytool -importkeystore -deststorepass changeit -destkeypass changeit -destkeystore out/server-multilevel-full.jks -srckeystore server-full.p12 -srcstoretype PKCS12 -srcstorepass changeit -alias server
```

