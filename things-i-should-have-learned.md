# Things I Should Have Learned By Now (Scala Edition)

This is a concise capture of the points raised in [this Scala Days 2019 talk](https://portal.klewel.com/watch/webcast/scala-days-2019/talk/49/)
by Nicolas Rinaudo. I just captured the lessons here; see the video for full context and
explanations as to WHY you should do these things in your Scala code.

Nicolas also made the following resources available:
Slides: https://nrinaudo.github.io/talk-scala-best-practices
Articles about these items: https://nrinaudo.github.io/scala-best-practices/

### Use sameElementsAs to compare Array objects, not ==
The following is true: `Array(1) != Array(1)`

### Explicitly define types of public members
This frequently bites me in the REPL with `Option`.

Bad: `def toOption[A](a: A) = Some(a)`

Good: `def toOption[A](a: A): Option[A] = Some(a)`

### Prefer ASCII operators to Unicode
The rules for operator precedence work properly for ASCII, but you may get some unexpected results
if you use the Unicode versions of operators.

### Prefer sealed trait hierarchies over Enumeration
Scala's built-in enumerations stink. Use this pattern instead:
```
sealed trait Status
object Status {
  case object Ok extends Status
  case object FooError extends Status
}
```

### Always make extensions of sealed traits final or sealed
The `sealed` keyword limits _direct_ extension of the `sealed` type, but not indirect. So when
extending a `sealed` type in the same file, be sure to make all such extensions either `final` or
`sealed` themselves to prevent leakage.
```
sealed trait Foo
final class Bar extends Foo
```

### Prefer extending Product and Serializable for ADTs
Type inference can get ugly if you omit these from the sealed type that roots your ADT.
```
sealed trait Status extends Product with Serializable
object Status {
  case object Ok extends Status
  case object FooError extends Status
}
```
This helps with type inference:
```
List(Status.Ok, Status.FooError)
```

### Prefer final case classes
Bad: `case class Foo(i: Int)`

Good: `final case class Foo(i: Int)`

### Avoid custom extractors
But if you can't, make sure they return `Some` instead of `Option`, as in:
```
object ExtractSome {
  def unapply[A](s: Some[A]): Some[A] = s
}
```

### Prefer type classes to structural typing
Troublesome:
```
def add(x: {def get: Int}) = 1 + x.get
final case class Wrapper(i: Int) {
  def get: Int = i
}
```
Preferred:
```
trait HasGet[A] {
  def get(a: A): Int
}
implicit val hasGetWrapper: HasGet[Wrapper] = new HasGet[Wrapper] {
  def get(a: Wrapper): Int = a.get  // or a.i
}
def add1[A](x: A)(implicit hga: HasGet[A]): Int = hga.get(a) + 1
```

### Prefer Option/Either/Try to throwing Exceptions
Not really news, but included for completeness.
  - `Option`: for when a value is potentially absent
  - `Either`: for when you want to convey the result of a potentially failing computation
  - `Try`: if you have to deal with code that throws (e.g., Java interop)

### Prefer extending Exception for an error ADT
**NOTE:** This isn't so that you can throw them, it just helps with interop when using code that
can throw exceptions.
```
sealed trait DbError extends Error with Product with Serializable
object DbError {
  case object InvalidSql extends DbError
  case object ConnectionLost extends DbError
}
```
For example, you can then insert your ADT where an `Exception` is required:
```
scala.concurrent.Future.failed(DbError.InvalidSql)
```

### Don't use the return keyword
I think this slide got eaten when viewing the presentation at the link above, but its pretty
straightforward. I don't think I've ever used `return` in any Scala code anyway.

### Prefer explicit conversions to implicit ones
If your conversion is not total, it can lead to code that compiles but blows up at runtime. For
example, this is a bad implicit conversion:
```
implicit def strToInt(s: String): Int = Integer.parseInt(s)
```

### For implicit resolution, prefer very specific names, including types
Names of implicits can clash, preventing proper resolution. Sometimes these errors are not always
clear.

### Prefer string interpolation over string concatenation
The `+` operator does not always behave as expected. Do this instead:
```
val foo = "FOO"
s"${foo}bar"
```
Alternatively, explicitly call `toString`.

Interpolation is faster than concatenation anyway. Win-win.
