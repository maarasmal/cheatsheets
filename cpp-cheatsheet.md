# C/C++ Tidbits

### Some GoogleTest commands

One of our projects implements unit tests using [GoogleTest](https://github.com/google/googletest).

Assume we have a test suite `FooSuite` with tests `FooInsertSuccess` and `FooDeleteSuccess`.

To run a single test:

```
/path/to/test-executable --gtest_filter=*FooInsertSuccess*
```

To run all tests within a suite:

```
/path/to/test-executable --gtest_filter=FooSuite*
```

To see all of the `gtest` options available:

```
/path/to/test-executable --help
```

### Get the current time

```
    // Get the current time in UTC seconds.
    std::chrono::time_point<std::chrono::system_clock> currentTimeChronoSeconds =
        std::chrono::time_point_cast<std::chrono::seconds>(std::chrono::system_clock::now());
    std::time_t currentTime = std::chrono::system_clock::to_time_t(currentTimeChronoSeconds);
```

Isn't that just wonderfully clear and "readable"?


