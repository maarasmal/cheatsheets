# Random notes about things from cats and cats-effect

## Random symbolic operators
[Love 'em, hate 'em, look them up here](https://typelevel.org/cats/faq.html#symbol)

**Examples**

| Sample | Description |
|---|---|
| `IO(a) *> IO(b)` | Compose the two actions, discarding the value of the former |
| `IO(a) <* IO(b)` | Compose the two actions, discarding the value of the latter |
| `IO(a) >> IO(b)` | Action `a` followed by action `b`. Similar  to `*>`, but `fb` is by-name (lazy) |

## flatTap
```
fa.flatTap(f)
```

This is an alias for

```
fa.flatMap(a => f(a).map(_ => a))
```

That is, run some effect but retain the original value.

```
@ Option(23).flatTap{x => println("foo" + x); Some(2)}
foo23
res26: Option[Int] = Some(23)
```

## Split List[Either[A, B]] into a Tuple2[List[A], List[B]]
This arose when I had a function returning `Either[A,B]`, and I wanted to call that for a
collection, then split out the errors from the passes.

Something like:

```
@ (1 to 10).toList.map(x => if(x % 2 == 0) Left(s"Error: $x") else Right(x)).separate
res50: (List[String], List[Int]) = (List("Error: 2", "Error: 4", "Error: 6", "Error: 8", "Error: 10"), List(1, 3, 5, 7, 9))
```

## Transform List[Either[String, A]] into Either[NonEmptyList[String], List[A]]
???

## "Flatten" a List[(List[A], List[B])] into (List[A], List[B])
Given:
```
@ x
res14: List[(List[Int], List[Int])] = List((List(1, 2), List(3, 4)), (List(6, 7), List(8, 9)))
```
Here is the gross way to do it:
```
@ x.foldLeft((List.empty[Int], List.empty[Int]))((accum, i) => (accum._1 ::: i._1, accum._2 ::: i._2))
res15: (List[Int], List[Int]) = (List(1, 2, 6, 7), List(3, 4, 8, 9))
```
And here is the handy Cats way (requires `import cats.implicits._`):
```
@ x.combineAll
res16: (List[Int], List[Int]) = (List(1, 2, 6, 7), List(3, 4, 8, 9))
```

## Combining Options with Parallel
I had this situation:
```
val a: Option[A] = ???
val b: Option[B] = ???
val c: Option[C] = ???

case class Foo(a: A, b: B, c: C)
val f: Option[Foo] = ??? // some magic here that combines the values a, b and c above
```

The magic that I was looking for is provided by `cats.Parallel` and its `mapN` function:

```
@ (a,b,c).mapN{ case (x,y,z) => println(s"Got values: $x, $y, $z"); x+y+z }
Got values: 1, 2, 3
res9: Option[Int] = Some(6)

@ val n: Option[Int] = None
n: Option[Int] = None

@ (a,n,c).mapN{ case (x,y,z) => println(s"Got values: $x, $y, $z"); x+y+z }
res11: Option[Int] = None
```
