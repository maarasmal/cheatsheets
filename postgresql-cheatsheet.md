# Postgresql Tips and Tricks

### Disable paging in psql
```
\pset pager off
```

### Enable timing of queries in psql
```
\timing
```

### View the possible values of an enumeration type:
```
SELECT unnest(enum_range(NULL::my_enum_type))
```
