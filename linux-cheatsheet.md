# Handy Linux Commands

This is a totally random grab-bag of stuff I have collected over the years. I have a bunch of
scattered notes all over with tips for various shell commands. As I consolidate them here, this
page might get a little more organized.

There is a pretty good [Bash scripting cheatsheet](https://devhints.io/bash) that someone else
wrote.

My long-time go-to guide on [Bash scripting](https://www.tldp.org/LDP/abs/html/).

Instead of an old-school shebang:

```
#!/bin/bash
```

You should prefer this for better portability (it will pick up the first `bash` on the `$PATH`
if the user happens to have multiple versions installed):

```
#!/usr/bin/env bash
```

OTOH, I've been advised that this can be a security issue, since it can invoke a bash executable
that is compromised, rather than the standard `/bin/bash`.

## General Commands

**NOTE:** Some of the commands in the following table have a `\` where they ought to have a `|`.
This is due to a parsing problem in markdown, since tables are delimited by `|` as well. Such
commands are prefixed with the non-fixed-width string "XYZ" until I have time to work out a better
solution.

| Command | Description |
|---|---|
| XYZ `echo -n deadbeef0102030405060708 \ xxd -r -p > output-file.bin` | Write a small hex value to a binary data file. Useful for creating dummy key files and things of that nature. Found this tip [here](https://stackoverflow.com/questions/1604765/linux-shell-scripting-hex-string-to-bytes) |
| `openssl rsa -in privkey.pem -pubout > key.pub` | Extract a public key from a private PEM file |
| XYZ `getent group foo \ cut -d: -f3` | Print the GID of the group `foo` |
| `dd if=/dev/<sourcedevice> of=/path/to/output.iso` | Rip a DVD image to an ISO file |
| `tar -xzf tarball.tgz --wildcards --no-anchored ‘desired-file*rpm’` | Unpack a single file from a `.tgz` file. **NOTE:** it will unpack the full path to the file, so if desired-file-1.0.1.rpm is located under `foo/bar`, running the command above will result in the creation of the file `foo/bar/desired-file-1.0.1.rpm` relative to the directory where you ran the command. |
| `xmllint --format infile.xml > outfile.xml` | Pretty-print the file `infile.xml` and write it out to `outfile.xml`. |
| XYZ `echo '{"foo": "lorem", "bar": "ipsum"}' \ python -m json.tool` | Pretty-print raw JSON data |
| `wget --http-user=username --http-password=password 'DownloadURL1' 'DownloadURL2' ...` | Download a large file with `wget` instead of a download manager. **NOTE:** URLs **must** be wrapped in single quotes |
| `builtin foo` | Runs the shell built-in named `foo`. This is useful when defining functions or aliases that have the same name as a shell built-in. For instance, you can alias the `logout` command to do some cleanup before it actually logs you out: `alias logout='~/bin/clean-temp-files.sh && builtin logout'` (NOTE: if the cleanup script there fails, you will not actually be logged out) |
| `date -u` | Prints the date and time in UTC. SUPER handy when you are looking at logs that use UTC for the timestamps instead of local time. |
| `unzip myfile.zip *.pem` | Extract all of the `.pem` files contained in the zip file `myfile.zip` |
| `telnet 192.168.1.1 1234` | Telnet to host `192.168.1.1` on port `1234`. Useful for verifying that a server is listening at the desired place. |
| `/usr/libexec/java_home -v1.8` | On **macOS**, prints the `JAVA_HOME` directory for the specified JDK. Useful if you have multiple JDKs installed. |
| `/usr/libexec/java_home -v11` | Same as above, but I think for versions 9 and higher, you don't use the `1.` prefix. |
| `curl 'http://localhost:8080/api/resource/SOMEKEY' -X PUT -H 'Content-Type: text/plain;charset=UTF-8' --data '@./req-body.json'` | Sample `curl` request showing a URL for a PUT request, a sample header (you probably need to add others) and how to specify a text file as the request's body. |
| `grep -rl string1 /path/to/files | xargs grep -l string2` | Recursively search all files under `/path/to/files` and print all files that contain both `string1` and `string2`. Chaining additional `xargs grep -l` pipes will let you search for more than 2 strings. |
| `kill -9 $(ps aux | grep 'some-pesky-app' | awk '{print $2}')` | Search the output of `ps` for `some-pesky-app` and kill it. The `awk` command prints out the 2nd token from the input that is piped to it. |


## Useful .bashrc entries
```
# Skip these files/directories when tab-completing
export FIGNORE=.svn:.git:.DS_Store

# Set prompt color
NO_COLOR="\[\033[0m\]"
__RED="\[\033[1;31m\]"
__GREEN="\[\033[1;32m\]"
__YELLOW="\[\033[1;33m\]"
__BLUE="\[\033[1;34m\]"
__MAGENTA="\[\033[1;35m\]"
__CYAN="\[\033[1;36m\]"


__LIGHT_GRAY="\[\033[1;37m\]"
__DARK_GRAY="\[\033[1;90m\]"
__LIGHT_RED="\[\033[1;91m\]"
__LIGHT_GREEN="\[\033[1;92m\]"
__LIGHT_YELLOW="\[\033[1;93m\]"
__LIGHT_BLUE="\[\033[1;94m\]"
__LIGHT_MAGENTA="\[\033[1;95m\]"
__LIGHT_CYAN="\[\033[1;96m\]"
__WHITE="\[\033[1;97m\]"
export PS1="${__GREEN}${PS1}${NO_COLOR}"
```

## System Administration Commands
| Command | Description |
|---|---|
| `useradd --create-home --gid foo --groups foo,bar jdoe` | Create a new user `jdoe` with primary group `foo`, but also in group `bar` |
| `usermod -l newname oldname` | Renames the account `oldname` to `newname` |
| `usermod -l newname -m -d /new/home/dir oldname` | Renames the account `oldname` to `newname` and also moves their home directory to the specified location |
| | **NOTE:** Renaming a user account will not affect things like entries in `/etc/auto.home` for `autofs` or entries in `/etc/sudoers`. |
| `usermod --groups foo,bar,baz jdoe` | Set the groups for `jdoe` to the specified set. If they are already in `foo` and `bar`, this command would add them to the `baz` group. |
| `userdel -r johnsmith` | Delete the user `johnsmith`, including their home dir |
| `groupadd -g 9999 newgroup` | Create the group `newgroup` with an explicit GID of 9999. |
| `groupmod -n newname oldname` | Renames the group `oldname` to `newname`. If the OS uses personal groups by default, you will probably want to do this if you rename as user as above. |
| `sudo pam_tally2 --user=jdoe --reset` | Unlocks the account `jdoe` that has been locked by PAM for too many login failures in a row. The number of failures allowed is defined in `/etc/pam.d/password-auth` |
| `ntpq -p` | Print NTP daemon configuration. Shows which NTP server you are talking to. |
| `tcpdump -w 0001.pcap -i eth0` | Capture packets from eth0 and write them to a PCAP file (this file can fed to Wireshark or read by `tcpdump`) |
| `tcpdump -r 0001.pcap` | Print the content in a PCAP file |
| `tcpdump -i eth0 port 443` | Capture only packets coming in on port 443 |
| `tcpdump -i eth0 src 10.1.1.1` | Capture only packets coming from the source IP 10.1.1.1 |
| `iptables -L` | List all rules in all chains |
| `sysctl <variable-name>` | Query the value of a sysctl variable (these will typically be referred to like `net.ipv4.conf.default.rp_filter`) |
| `sudo sysctl -w var=value` | Set the sysctl variable `var` to the value `value`. Probably will not survive a reboot. Better to edit `/etc/sysctl.conf`, or put something in `/etc/sysctl.d` |


## tcpdump is a big topic that might get its own dedicated cheatsheet...
This answer on SO was generally informative, especially the 3 bullet points at the end:
https://unix.stackexchange.com/a/207565/172305

## CentOS Specific Commands
Sometimes you need to just download an RPM file rather than install it. You can install `yum`
plugins to help with this.

### Download a single package and its dependencies
Using `yumdownloader`, as described [here](https://access.redhat.com/solutions/10154)
```
sudo yumdownloader --resolve <package-name-1.0.1>
sudo yumdownloader --resolve <package-name>
```

### Download lots of updates
`yumdownloader` is handy for getting a single package updated. But if you want to get ALL updates
downloaded for the current system, you will want to use the `downloadonly` plugin:

```
# Install the plugin
yum install yum-plugin-downloadonly

# Run it for a single package (instead of yumdownloader):
yum install --downloadonly --downloaddir=<directory> <package>

# Get ALL updates for the entire system and only download them:
yum upgrade --downloadonly --downloaddir=<directory>
```

### CentOS 7 console resolution
When running CentOS in a VM, I wanted to make the console size bigger.

1. As `root`, edit `/etc/default/grub`
2. Edit the entry for `GRUB_CMDLINE_LINUX`
3. Add the following option to that entry: `vga=ask`
4. Save and close the file
5. Run `grub2-mkconfig -o /boot/grub2/grub.cfg`
6. Restart the server
7. When prompted `Press <Enter> to see video modes available`, press `<Enter>`.
8. Make a note of the 3-hexadecimal value next to the resolution you wish to use.
9. Enter a value [0-9a-f] to select a resolution for this boot. We will then update the GRUB
   config to use a fixed resolution by default.
10. Again edit `/etc/default/grub` as `root`
11. Edit the `vga=ask` option to instead be `vga=0xN`, where N is the hexadecimal number from the
    boot screen that you noted in step 8. For example, if you noted the value `345` on the boot
    screen, you would set the option to be `vga=0x0345`
12. Run `grub2-mkconfig -o /boot/grub2/grub.cfg`
13. Restart the server

On a 15" MBP screen, 1152x864x32 seemed reasonable (note that with VirtualBox, you will want to
scale the video to 200% to make it readable on the built-in monitor). On a larger (or external)
monitor, 1280x1024 or even 1600x1200 might work out ok. (On my external monitor with VirtualBox,
I set the video scaling to 100%. PITA when swapping the window to a different monitor, but at
least it works)

### Random database stuff
```
sudo -u postgres psql mydb -c 'select * from mytable'

su - postgres
psql
select MD5('mypasswordusername');  <-- NOTE: concatenate 'mypassword' and 'username'!!!
Then, in the 'create user' command, you put "md5" at the beginning of the value from the
command above.

(as the hsqldb user): java -jar /usr/local/hsqldb/lib/hsqldb.jar myhsqldb

```

### Random SVN stuff
Checkout just the top-level of a repository (to save space, or skip files you know you don't need)
```
svn checkout --depth immediates <repository-url> my-repo
```

If you then want to check out the full contents of the `foo` directory inside the repo you just
checked out:
```
cd my-repo/foo
svn update --set-depth infinity
```

If you later want to get rid of the files in just that `foo` directory, you can run:
```
cd my-repo/foo
svn update --set-depth empty
```

### Parsing JSON on the command line
The tool `jq` is handy for pretty printing JSON, but can also allow you to parse JSON in your
scripts. For example:

```
HQSML-1612683:$ cat foo.json
[
  {
    "id": "1d0c432c-2b5b-44f1-9ece-9931215a063d",
    "index": {
      "shortLabel": "3",
      "index": 3
    },
    "name": "SOMENAME4"
  },
  {
    "id": "4188c22f-3045-44cc-926f-5e8bc1e6a654",
    "index": {
      "shortLabel": "1",
      "index": 1
    },
    "name": "SOMENAME2"
  }
]
HQSML-1612683:$ cat foo.json | jq -r '.[] | .name'
SOMENAME4
SOMENAME2
HQSML-1612683:$ cat foo.json | jq -r '.[] | .index | .shortLabel'
3
1
```

