# Terraform

- Variables go in a file named `terraform.tfvars`.
- Secrets shouldn't be in your terraform config or tfvars file, but you can put them somewhere
  like a `secrets.tfvars` file and manually pass that to terraform:
  ```
  terraform apply -var-file="secrets.tfvars" -var-file="production.tfvars"
  ```
- Simple string variables can be defined via environment variables. The env var `TF_VAR_foo` will
  be substituted for the `foo` variable in your configuration.


| Command | Description |
|---|---|
| `terraform init` | Initializes your terraform "workspace", downloading required plugins for providers |
| `terraform plan` | Generates a "diff" of your current config against the last known state of your deployment |
| `terraform apply` | Applies the current configuration to the defined systems |
| `terraform fmt` | Auto-formats your .tf files |
| `terraform validate` | Validates the configuration |
| `terraform show` | Display the current state of the configuration (reads the .tfstate file) |
| `terraform state list` | List the resources in the current state (useful if `show` output would be large and unwieldy |
| `terraform destroy` | The opposite of `apply`, it destroys all of the resources in the configuration. Note that if you just need to, say, go from resources ABC to just AB, you can just delete resource C from the  config and run `apply`. Only resource C will be destroyed. |

