# Monocle Reference

## Lens

`Lens[S, A]`

| Function | Shape |
|---|---|
| `get` | `S => A` |
| `set` | `A => S => S` |

---

## Optional

`Optional[S, A]`

| Function | Shape |
|---|---|
| `getOption` | `S => Option[A]` |
| `set` | `A => S => S` |

---

## Prism

`Prism[S, A]`

| Function | Shape |
|---|---|
| `getOption` | `S => Option[A]` |
| `reverseGet (apply)` | `A => S` |

---

## Traversal

`Traversal[S, A]`

[Documentation](https://www.optics.dev/Monocle/optics/traversal.html)

| Function | Shape | Description |
|---|---|---|
| `set` | `A => S => S` | Sets all elements in `S` to the value `A` |
| `modify` | `(A => B) => S => S` | Applies a function to each element in `S` |
| `getAll` (from `Fold`) | `S => List[A]` | Retrieves the collection from within the `S` |
| `headOption` (from `Fold`) | `S => Option[A]` | Optionally retrieves first element from within `S` |
| `find` (from `Fold`) | `(A => Boolean) => S => A` | Finds the first `A` within `S` that satisfies a predicate |
| `all` (from `Fold`) | `(A => Boolean) => S => Boolean` | Determines whether all elements within `S` conform to a predicate |

Simple creation:

```
val xs = List(1,2,3,4,5)
val listTraversal: Traversal[List[Int], Int] = Traversal.fromTraverse[List, Int]
```

Composed creation:

```
@Lenses
final case class Foo(id: Int, b: Bar)
@Lenses
final case class Bar(id: Int, values: List[String])

val fooValuesTrav: Traversal[Foo, String] =
  Foo.b
    .composeLens(Bar.values)
    .composeTraversal(Traversal.fromTraverse[List, String])
```

Real world example encountered with Partial objects used to back UI components:

```
@Lenses
final case class PartialThingy(
  id: ThingyId
)

@Lenses
final case class PartialPlant(
  pid: PlantId,
  thingyId: Option[ThingyId]
)

@Lenses
final case class PartialBar(
  id: BarId,
  plants: List[PartialPlant]
)

@Lenses
final case class AFoo(
  id: FooId,
  bars: List[PartialBar]
)

@Lenses
final case class BFoo(
  id: FooId,
  plants: List[PartialPlant]
)
```

We had a case on the Foo Controller Details screen where Plants in a Foo may refer to one of the `Thingys` defined for the controller. If a `Thingy` is deleted, any `Plant` that refers to that `Thingy` should have that field set to `None`. For `AFoo` controllers, this is a little extra complicated because `AFoo`s have a set of `Bar` objects, each of which has its own list of `Plant`. (`BFoo` objects directly contain the list of `Plant`) So the first cut at the function to go through and update everything for an `AFoo` looked like this:

```
private def onAFooThingyDelete(t: PartialThingy): Callback =
  scope.modState { s =>
    val newValue = s.value.copy(bars = s.value.bars.map { bar =>
      bar.copy(plants = bar.plants.map { plnt =>
        if (plnt.thingyId.exists(_ === t.id)) plnt.copy(thingyId = None) else plnt
      })
    })
    s.copy(value = newValue)
  }
```

Gross, right? After flattening the `Partial` types used by the screen today, the Monocle optic situation was greatly improved, so I thought maybe `Lens` would make things easier:

```
private def onAFooThingyDelete(t: PartialThingy): Callback =
  scope.modState { s =>
    aFooBarsLens.modify(bars =>
      bars.map{r =>
        PartialBar.plants.modify(plants =>
          plants.map{ plant =>
            if (plant.thingyId.exists(_ === t.id)) plant.copy(thingyId = None) else plant
          }
        )(r)
      }
    )(s)
```

A little better, maybe? Arguably easier to read without the calls to `copy` in there, but I wasn't thrilled with it. Then I recalled seeing something called a `Traversal` when browsing the Monocle docs over the last week. (https://www.optics.dev/Monocle/optics/traversal.html) Using some `Lens` objects that were already available, I added this guy:

```
private val aFooPlantTraversal: Traversal[State, PartialPlant] =
  aFooBarsLens
    .composeTraversal(Traversal.fromTraverse[List, PartialBar])
    .composeLens(PartialBar.plants)
    .composeTraversal(Traversal.fromTraverse[List, PartialPlant])
```

Then used it to implement `onAFooThingyDelete`:

```
private def onAFooThingyDelete(t: PartialThingy): Callback =
  scope.modState { s =>
    aFooPlantTraversal.modify{ plant =>
      if (plant.thingyId.exists(_ === t.id)) plant.copy(thingyId = None) else plant
    }(s)
  }
```

The `if` statement in there was redundant with the one used in a similar method used for BFoo controllers, so after factoring that out to its own method `maybeClearThingy`, the final implementation is:

```
private def onAFooThingyDelete(t: PartialThingy): Callback =
  scope.modState(s => aFooPlantTraversal.modify(maybeClearThingy(_,t.id))(s))
```

## Embedding

`Embedding[A, B]`

This type is not part of Monocle, but it is inspired by `Prism` and is very similar.

Per Peter L.:
> The thing about Embedding is to remember the left side is the multiplicity of possible strings that map to a SINGLE value in the right-side datatype

Note that he's referring to how our project mainly used `Embedding`, which was as `Embedding[String, T]`.

| Function | Shape |
|---|---|
| `extract` | `A => Option[B]` |
| `embed` | `B => A` |
