# Monad Transformers in Scala

Mark Nelson came up with this handy little list, I just formatted it a bit

## Option[A]
| What you have | What you need | What to call |
|---|---|---|
| `A` | `OptionT[F, A]`  | `OptionT.some` |
| `F[A]` | `OptionT[F, A]` | `OptionT.liftF` |
| `Option[A]` | `OptionT[F, A]` | `OptionT.fromOption` |
| `F[Option[A]]` | `OptionT[F, A]` | `OptionT` constructor (apply) |
| `OptionT[F, A]` | `OptionT[F, B]` | `.map(A => B)` |
| `OptionT[F, A]` | `F[Option[A]]`  | `.value` |

## Either[A, B]
| What you have | What you need | What to call |
|---|---|---|
| `A` | `EitherT[F, A, B]` | `EitherT.leftT` |
| `B` | `EitherT[F, A, B]` | `EitherT.rightT` |
| `F[A]` | `EitherT[F, A, B]` | `EitherT.left` |
| `F[B]` | `EitherT[F, A, B]` | `EitherT.right` |
| `Either[A, B]` | `EitherT[F, A, B]` | `EitherT.fromEither` |
| `F[Either[A, B]]` | `EitherT[F, A, B]` | `EitherT` constructor (apply) |
| `Option[B]` | `EitherT[F, A, B]` | `EitherT.fromOption(o: Option[B], l: A)` |
| `F[Option[B]]` | `EitherT[F, A, B]` | `EitherT.fromOptionF(o: F[Option[B]], l: A)` |
| `EitherT[F, A, B]` | `EitherT[F, C, B]` | `.leftMap(A => C)` |
| `EitherT[F, A, B]` | `EitherT[F, A, D]` | `.map(B => D)` |
| `EitherT[F, A, B]` | `F[Either[A, B]]` | `.value` |

