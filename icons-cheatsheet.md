
# Octicons
https://primer.github.io/octicons/

ScalaJS React example definition:

```
import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

import japgolly.scalajs.react.{Children, JsComponent}

object MyIcons {

  @JSImport("react-octicons-svg/dist/Plus.js", JSImport.Default)
  @js.native
  object PlusComponent extends js.Object

  val plus = JsComponent[Props, Children.None, Null](PlusComponent)
      .apply((new js.Object).asInstanceOf[Props])

}
```

Then to use that component you can do something like:

```
  <.span(MyIcons.plus.vdomElement)
```

# Bootstrap Icons
https://icons.getbootstrap.com/

Example of using ScalaJS React with a bootstrap icon:
```
      <.i(
        ^.`class` := "bi bi-patch-question-fill",
        ^.color := "#f6be4f",
        ^.title := "This is the tooltip text"
      )
```
