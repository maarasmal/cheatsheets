# Design Principles

This is a collection of random design philosophies that I have collected through various channels
over the years, on a variety of topics. They are just generalized principles about how to design
and organize code.

## The Robustness Principle (Postel's Law)

Described [here](https://en.wikipedia.org/wiki/Robustness_principle). In a nutshell: "be
conservative in what you send, be liberal in what you accept". The idea sounds good at first
glance. I think that it often makes sense to apply to that ideal for internal designs, but I am not
so sure I agree with it with respect to public APIs.

Suppose ServerA supports ProtocolX (a protocol that is defined outside of the team that is
developing ServerA), and part of the protocol specification is a `request-id` that is defined to be
represented as a UUID. Should ServerA explicitly reject incoming requests that contain a
`request-id` that is not a UUID?

If every server supporting ProtocolX has to allow non-UUID values for `request-id`, doesn't the
`request-id` field lose all of its meaning? Applications will just stop using it, since it could be
"anything". Being _too_ liberal in what you accept can lead to a devaluing of the information that
is conveyed through the protocol.

Additionally, the first part of the principle implies that the _client_ should be conforming to the
protocol specification. The principle is not a get-out-of-jail free card that lets clients just do
whatever they darn well please.

There is a good section on [criticisms](https://en.wikipedia.org/wiki/Robustness_principle#Criticism)
of the principle on the wikipedia article. There are potential security risks that can arise from
not strictly adhering to public API specifications.

## On Deriving Encoders/Decoders For Formal APIs

> Using derived encoders/decoders on formal interfaces is a recipe for disaster

> If it is a formal interface, you have a defined schema on what JSON should look like

> Deriving an encoder/decoder instead relies on coming up with a case class / ADT definition that encodes/decodes to valid instances of the schema

> Even when you get it working, the primacy of the schema is gone

> So then a year from now, someone changes something about the case class or ADT and it breaks

## On Versioned API Schemas
> First we encourage that both parties abide by the robustness principle: "Be conservative in what you send, be liberal in what you accept"
> i.e. don't blow up if there's an unexpected field, don't perform schema validations on consumed representations. So if the client added new fields, or the server removed expected fields, there should be no issue. When the server requires new fields, or the client removes required fields, then changes becoming breaking. (edited) 
> but taking a step back: one of the major considerations about whether or not to implement a versioning scheme is whether you have control over the timing and coordination of client and server deployments. That is, if you had a need to support independent client upgrades without respect to server release timing, that would favor versioning. So if you have devices that are sold to customers that would talk to a central server, and each client may need to be upgraded on its own timeline, versioning may be important. In our case I believe that we should be able to coordinate updates to [the relevant] client code with [server API] updates without much trouble, leading me to believe that versioning is not as important here.
> Having said this, experience has taught us that there's usually not a good ROI when implementing versioning. A lot of work goes into implementing something that is never needed. Given this, most projects I've been involved with lately have made the decision not to implement a versioning strategy (i.e. conscious decision for versioning scheme = NONE).


