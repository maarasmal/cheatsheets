# Concourse Cheatsheet

Most of the stuff below pertains to `fly`, the CLI tool for interacting with Concourse. For the
pipeline definitions, your best bet is the
[official documentation](https://concourse-ci.org/jobs.html).

## Useful commands
List the targets are currently known to `fly`:
```
fly targets
```

Authenticate with your Concourse server:
```
fly --target tgt login --team-name myteam --concourse-url https://ci.example.com
```
This authenticates you AND saves the connection info under an "alias" of `tgt`. You will then
specify this alias in subsequent commands.

Set up a pipeline named `myproj` as defined in the `pipeline.yml` file of the current directory:
```
fly -t tgt set-pipeline --pipeline "myproj" --config "./pipeline.yml"
```

View the console output of the last time the job `myproj/build` was run:
```
fly -t tgt watch --job myproj/build
```

Pause / unpause the entire `myproj` pipeline:
```
fly -t tgt pause-pipeline -p myproj
fly -t tgt unpause-pipeline -p myproj
```

Pause / unpause just the `pr-build` job within the `myproj` pipeline:
```
fly -t tgt pause-job --job myproj/pr-build
fly -t tgt unpause-job --job myproj/pr-build
```

Inspect a Docker container for a build:
```
fly -t team intercept -j myproj/pr-build
```

Login options
```
fly -t tgt teams &>/dev/null || fly login -t tgt -c https://my.concourse.net -n myteam --ca-cert=concourse-ca.pem

fly -t tgt teams &>/dev/null || fly --target tgt login --concourse-url https://my.concourse.net --team-name myteam --open-browser
```


## Teams

A `team` is a conceptual owner/namespace and can own the following:

* pipelines
* builds
* other user data

## Pipelines
A pipeline is a related configuration of `Jobs` and `Resources` together. They are configured via
YAML files.

## Jobs
Each job has a single build plan. A `plan` is a sequence of _steps_ to execute. Steps are things
like `get`, `task` and `put`.


## pipeline.yml

- `((secret-name))` this variable will be replaced with the appropriate value from Vault.
  Concourse retrieves this value automatically for you.
  See: https://concourse-ci.org/vault-credential-manager.html

