# Helpful Luna-related commands

## The lunacm utility
Managing the card is done through the `lunacm` utility. Most of the examples in this section show
commands that should be entered into the `lunacm` prompt after launching it.

### Reset the Crypto Officer Password
If you have an unruly app that tries and fails to log in multiple times (say you have configured the
wrong password by accident), the card will get locked out. These steps will help you unlock the
card without destroying the data on it.

Enter the following in `lunacm`:
```
slot set -slot 1
role login -name SO
# Enter the SO password when prompted, then press <Enter>
role resetPW -name Crypto Officer -slot 0
```

### View HSM Attributes
```
slot set -slot 1
hsm showInfo
```

### View Partition Contents
```
slot set -slot 0
role login -name Crypto Officer
# Enter the Crypto Officer password and hit <Enter>
partition contents
```

## CMU
Certificate Management Utility
`/usr/safenet/lunaclient/bin/cmu`

### Viewing installed certificates
`cmu list`
list the certs on the Luna card. Prompts you to select a slot, for which you will need a password

### Exporting certificates
`cmu export`
Export a certificate from the card. An example export session looks like so:

```
[admin@myhost test-foo-certs]$ cmu export
Select token
 [0] Token Label: foorootca1
 [1] Token Label: foorootca1
 Enter choice: 0
Please enter password for token in slot 0 : ********
Enter output filename : encryptionkey--cert0.cer
Select Certificate to export

Handler	Label
33	signingkey--cert2
43	signingkey--cert1
82	sharedcodesigningkey--cert0
60	sharedsigningkey--cert0
51	signingkey--cert0
55	codesigningkey--cert0
48	encryptionkey--cert0
Enter handler (or 0 for exit) : 48
```

---

## Keytool

The Java `keytool` command can be used to perform operations on the HSM such as loading keys and
certificates.

**Note:** In all of the examples below `keystore.luna` is a plain text file that contains a single
line that reads: `slot:0`

### Listing the Contents of a Partition (partition specified in keystore.luna, in this case '0')
```
keytool -list -v -storetype Luna -keystore keystore.luna
```

### Importing a Key Pair from a PKCS12 Key Store:
```
keytool -importkeystore -srckeystore /tmp/src-store.p12 -srcstoretype PKCS12 -srcprovidername SunJSSE \
    -srcstorepass password1 -destkeystore keystore.luna -deststorepass <Crypto Officer Password> \
    -deststoretype Luna -destprovidername LunaProvider \
    -providerclass com.safenetinc.luna.provider.LunaProvider -providerpath LunaProvider.jar
```

### Creating an RSA Key Pair:
```
keytool -genkeypair -alias mykeypair -keyalg RSA -sigalg SHA256withRSA -keystore keystore.luna \
    -storetype Luna
```

### Root CA and Sub-CA Example (Key pair gen, self-signed cert creation, CSR generation, certificate from CSR)
1. Create the RSA key pair and a self signed cert:
```
keytool -genkeypair -alias my_root_ca_kt_2 -keyalg RSA -keysize 2048 -sigalg SHA256withRSA \
    -ext bc:critical=ca:true -ext ku:critical=keyCertSign \
    -dname "CN=Test My Root CA, O=\"MyCorp, Inc\", C=US" -validity 1825 -storetype Luna \
    -keystore keystore.luna
```
2. Export the certificate and visually inspect it
```
keytool -export -alias my_root_ca_kt_2 -file /tmp/outfile.cert -v -storetype Luna -keystore keystore.luna
openssl x509 -in /tmp/outfile.cert -inform der -text -noout
```
3. Create the sub-ca key pair and initial self-signed certificate:
```
keytool -genkeypair -alias my_sub_ca_kt_2 -keyalg RSA -keysize 2048 -sigalg SHA256withRSA \
    -ext bc:critical=ca:true,pathlen:0 -ext ku:critical=keyCertSign \
    -dname "C=US, O=My CA, CN=Test My sub-CA 1" -validity 1825 -storetype Luna -keystore keystore.luna
```
4. Export the self-signed sub-ca certificate and visually inspect it:
```
keytool -export -alias my_sub_ca_kt_2 -file /tmp/outfile_subca.cert -v -storetype Luna -keystore keystore.luna
openssl x509 -in /tmp/outfile_subca.cert -inform der -text -noout
```
5. Create a CSR for the sub-ca key pair
```
keytool -certreq -alias my_sub_ca_kt_2 -dname "C=US, O=My CA, CN=Test My sub-CA 1" \
    -sigalg SHA256withRSA -file /home/myuser/subca_kt.csr -storetype Luna -keystore keystore.luna
```
6. Create a certificate based on the CSR that was just generated
```
keytool -gencert -v -alias my_root_ca_kt_2 -rfc -sigalg SHA256withRSA -infile /home/myuser/myca/subca_kt.csr \
    -outfile /home/myuser/myca/subca_kt.pem -validity 1825 \
    -ext bc:critical=ca:true,pathlen:0 -ext ku:critical=keyCertSign -storetype Luna -keystore keystore.luna
```
7. Visually inspect the generated certificate
```
openssl x509 -in /home/myuser/myca/subca_kt.pem -text -noout
```
