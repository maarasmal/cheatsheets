# vim help

## The basics
The commands in the table below are entered in while you are in "navigate" mode, which is the
default state when you first open a file in `vim`. In "edit" mode you can type content directly
into your file "like a normal editor". A third mode is "command" mode, which is where you enter
commands at the prompt at the bottom of the `vim` window.

When you are in "edit" or "command" mode, press `esc` to return to "navigate" mode. 

### Command mode commands
To enter command mode, start by typing a `:`. This will open up the command input line at the
bottom of the editor.

| Command | Mnemonic | Description |
|---|---|---|
| `:q` | "quit" | Exit out of `vim` |
| `:w` | "write" | Write out the contents of the buffer to the file. I.e., "save". |
| `:q!` | | Quit without saving. WARNING: you will not be prompted, so you WILL lose any unsaved changes if you do this. |
| `:wq` | "save & exit" | Write out the buffer, then exit out of `vim` |

### Navigation mode commands
The following commands assume you are in navigation mode. If in doubt, press `esc` a couple times
to make sure you are out of edit or command mode before entering these commands.

Capitalization matters. `a` and `A` are related but behave significantly different.

| Command | Mnemonic | Description |
|---|---|---|
| `h`/`j`/`k`/`l` | | Move the cursor one character left/down/up/right (respectively) |
| `i` | "insert" | Enter 'edit' mode at the current cursor location |
| `I` | "Insert" | Enter 'edit' mode at the start of the current line |
| `a` | "append" | Enter 'edit' mode at the position just after the current cursor location |
| `A` | "Append" | Enter 'edit' mode at the end of the current line |
| `o` | "open" | Open a new line below the current one, in 'edit' mode |
| `O` | "Open" | Open a new line above the current one, in 'edit' mode |
| `w`/`b` | "word" | Move the cursor forward or backwards one word |
| `x` | "cut" | Delete the character at the current cursor position |
| `X` | "Cut" | Delete the character immediately to the left of the current cursor position |
| `s` | "substitute" | Delete the character at the current cursor position and then enter 'edit' mode |
| `S` | "Substitute" | Delete the entire current line and then enter 'edit' mode |
| `C` | "change" | Delete from the cursor to the end of the line, then enter 'edit' mode |
| `u` | "undo" | Undo the last edit. Can be repeated. |
| `ctrl` + `r` | "redo" | Redo the last thing you undid. Can be repeated. |
| `ctrl` + `f` | "forward" | Move one screen forward through the file |
| `ctrl` + `b` | "backward" | Move one screen backward through the file |
| `:1` | | Go to line 1. Any number can be put here to jump to a line. So `:23` will jump the cursor to line 23 |
| `G` | | Go to the bottom of the file |
| `M` | "middle" | Move the cursor to the line that is currently at middle of the screen |
| `/foo` | "search" | Search forward from the current cursor location for the text "foo". The search string here can be a regex. (e.g., `/foo.*bar`) |
| `?foo` | "backwards search" | Search backwards from the current cursor location for the text "foo".  The search string here can be a regex. (e.g., `?foo.*bar`) |
| `shift` + `8` | | Search for the word that the cursor is currently on. |
| `n` | "next" | Jump the cursor to the next item that matches your last search. **NOTE:** This will move you in the direction of your last search. That is, if you searched with `/foo`, `n` will move you forward to the next match. If you searched with `?foo`, `n` will move you backwards to the next match. |
| `N` | "anti-next" :P | Jump the cursor back to the previous item that matches your last search. This behaves similarly to `n`, in that it is relative to the original search direction. That is, if you search with `?foo`, `shift + n` will move to the next match FORWARD from the cursor. |
| `%` | "jump to match" | Use this while the cursor is on a paren, bracket, curly brace, etc. to jump to the matching element. |

## Slightly advanced editing
Here, the notation `a`,`b` means "press a, then press b". These commands are like compound commands, where you are combining multiple ideas from above, or repeating a command from above some number of times. For commands that start with a number below, that number can be any number. The command specified after the number will be repeated that many times.

| Command | Mnemonic | Description |
|---|---|---|
| `d`,`d` | "delete line" | Delete the current line |
| `d`,`w` | "delete word" | Delete from the current cursor position to the end of the word that is currently under the cursor |
| `c`,`w` | "change word" | Delete from the current cursor position to the end of the word that is currently under the cursor, then enter 'edit' mode |
| `7`,`x` | | Delete 7 characters starting from the current cursor position. The number can be any number. To delete 23 characters, enter `2`,`3`,`x` |
| `7`,`s` | | Delete 7 characters starting from the current cursor position and then enter 'edit' mode. The number can be any number. |
| `7`,`S` | | Delete 7 lines starting from the current cursor position, then enter 'edit' mode. The number can be any number. |
| `7`,`d`,`d` | | Delete 7 lines. The number can be any number. |
| `3`,`c`,`w` | | Delete from the current cursor position to the end of the 3rd word, starting from the word the cursor is currently on, then enter 'edit' mode. The number can be any number. |

The `.` (period) command will repeat the last edit you did, no matter what it was, at the current cursor location. If you typed `d`,`d`, `.` will then delete a second line. If you hit `A`, then typed "</some-tag>", then hit the `esc` key, pressing `.` will append the text "</some-tag>" to the end of whatever line your cursor is on when you hit `.`.

The `v` key will put the cursor in VISUAL mode. You can move the cursor around to highlight irregular chunks of text, even across lines. Once you have highlighted text, you can use other commands shown above to manipulate it. So `x` will cut the highlighted text, as will `d`. `s` will "substitute" the text by deleting it and putting you into edit mode.

## Search and Replace
**TODO**

## Vim configuration
This is what I use as a basic minimal `~/.vimrc` file. Pretty basic stuff, but it works for me.

**TIP:** To avoid annoyances, if you are copy/pasting this into your file, open the file in `vim`,
then run the command `:set paste`, then copy/paste the text below. Entering paste mode disables
some annoying defaults related to wrapping text, auto-indentation and auto-continuation of comments.

```
set nocompatible

" Syntax highlighting
syntax on

" Highlight text that matches search strings
set hlsearch

" How to save line endings
set fileformats=unix,dos,mac

" How <tab> works in auto-completing
set wildmenu
set wildignore=*.dll,*.o,*.obj,*.bak,*.exe,*.pyc,*.jpg,*.gif,*.png
set wildmode=list:longest

" Disable automatic text wrapping
set fo-=t

" Show cursor info at the bottom of the screen
set ruler

" Scroll when the cursor is this many lines from the top/bottom of the screen.
" Provides context when reading/searching
set scrolloff=7

" Highlight matching parentheses, brackets, etc.
set showmatch

" Yes, I use tabs at 2 spaces because Scala. Most people will want these all set to 4.
set expandtab
set shiftwidth=2
set softtabstop=2
set tabstop=2

" Disable automatic continuation of comments when you hit ENTER or o/O. This feature is SUPER
" annoying when you paste (via the host OS, not vim's own p/P command) a chunk of code that
" includes a comment.
" I don't understand all this autocmd stuff, but you can also do 'set fo-=r' in the app to
" configure the same option. (Handy when pasting in this giant chunk of text on a new system)
autocmd BufNewFile,BufRead * setlocal formatoptions-=r

" Sets the background color of text beyond 100 characters on a line to light grey. This is handy
" for detecting when you might want to think about splitting up a line
augroup vimrc_autocmds
  autocmd BufEnter * highlight OverLength ctermbg=lightgrey guibg=#592929
  autocmd BufEnter * match OverLength /\%100v.*/
augroup END
```
