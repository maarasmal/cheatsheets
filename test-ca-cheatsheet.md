
# Setting Up A Test CA
---

Step 1. Download and install openssl. Add the openssl bin directory to the PATH variable. Alternatively, you can install openssl via cygwin.

**Note:**
> You can also go to www.openssl.org and download the source code.  Even Windows users need to build it, so you will need access to a C compiler.  You may be able to get hold of prebuilt binaries on the web (http://www.openssl.org/related/binaries.html) and you can certainly get hold of the GNU C compiler or you can use Borland and Microsoft compilers. There are good build instructions included with the source distribution, so this guide will not cover that.


Step 2.  Create directories to hold your CA keys, your server keys and, if
you want to use SSL client authentication, your client keys.  For the sake
of argument, we assume that these directories are called "ssl/ca",
"ssl/server" and "ssl/client".

Step 3.  Create a private key and certificate request for your own CA (NOTE: If a second numeric OU field is required in this certificate for a Vendor Index or otherwise, follow instructions at bottom for setting up a cetificate request with a vendor index):
`openssl req -new -newkey rsa:2048 -nodes -out ssl/ca/ca.csr -keyout ssl/ca/ca.key`

Step 4.  Create a self-signed certificate for the CA (note: lasts one year -
increase the days setting to whatever you want):
`openssl x509 -trustout -signkey ssl/ca/ca.key -days 365 -req -in ssl/ca/ca.csr -out ssl/ca/ca.pem`

WINDOWS USERS:If you copy the ca.pem file to ca.cer and edit the file so
that the strings "TRUSTED CERTIFICATE" read "CERTIFICATE", you can import
your CA certificate into your trusted root certificates store.

Step 5.  Import the CA certificate into the JDK certificate authorities (System wide) keystore. This step is important because the java keytool needs to know about the trusted certificate when you import certificates in order to create the appropriate certificate chains.
`keytool -import -keystore $JAVA_HOME/jre/lib/security/cacerts -file ssl/ca/ca.pem -alias my_ca`

Windows users need to replace $JAVA_HOME with %JAVA_HOME%.  Also if the %JAVA_HOME% includes a space character as in "Program Files", you will need to wrap that entire value in double quotes, as in:
`keytool -import -keystore "%JAVA_HOME%/jre/lib/security/cacerts" -file ssl/ca/ca.pem -alias my_ca`

NOTE: when asked "Enter keystore password:", enter "changeit" (without the quotes).

When asked "Trust this certificate? [no]", answer "yes".

Step 6.  Create a file to hold the serial numbers for the CA.  This file starts
with the number "2":
`echo 02 > ssl/ca/ca.srl`

# SETTING UP YOUR WEB SERVER
---

Step 7.  Create a keystore for your web server.
`keytool -genkey -alias tomcat -keyalg RSA -keysize 2048 -keystore ssl/server/server.ks -storetype JKS`

Step 8.  Create a certificate request for your web server.
`keytool -certreq -keyalg RSA -alias tomcat -file ssl/server/server.csr -keystore ssl/server/server.ks`
You need to edit the certificate request file slightly.  Open it up in a
text editor and amend the text which reads "NEW CERTIFICATE REQUEST" to
"CERTIFICATE REQUEST"

Step 9.  Have your CA sign your certificate request:
`openssl x509 -CA ssl/ca/ca.pem -CAkey ssl/ca/ca.key -CAserial ssl/ca/ca.srl -req -in ssl/server/server.csr -out ssl/server/server.cer -days 365`

Step 10.  Import your signed server certificate into your server keystore:
`keytool -import -alias tomcat -keystore ssl/server/server.ks -trustcacerts -file ssl/server/server.cer`
You should see a message "Certificate reply was installed in keystore".

Step 11.  Import your CA certificate into your server keystore:
`keytool -import -alias my_ca -keystore ssl/server/server.ks -trustcacerts -file ssl/ca/ca.pem`
This step is only necessary if you wish to use SSL client authentication
with Tomcat.

Step 12. Set up an SSL connector for Tomcat.  I assume that you know, or can
find out, how to do this.  Open up conf/server.xml in a text editor and
search for the text "keystoreFile".  Ensure that the attribute value is the
keystore you have created above.

# SETTING UP A SSL BROWSER CLIENT with Client Authentication Enabled.
---

Step 13.  Create a client certificate request:
`openssl req -new -newkey rsa:2048 -nodes -out ssl/client/client1.req -keyout ssl/client/client1.key`
The common name of the client must match a user in the Tomcat user realm (e.g.  an entry in `conf/tomcat-users.xml`). The keysize can also be changed to 1024, 2048, etc.

Step 14.  Have your CA sign your client certificate.
`openssl x509 -CA ssl/ca/ca.pem -CAkey ssl/ca/ca.key -CAserial ssl/ca/ca.srl -req -in ssl/client/client1.req -out ssl/client/client1.pem -days 365`

Step 15.  Generate a PKCS12 file containing your server key and server
certificate.
`openssl pkcs12 -export -clcerts -in ssl/client/client1.pem -inkey ssl/client/client1.key -out ssl/client/client1.p12 -name "my_client_certificate"`

Step 16.  Import the PKCS12 file into your web browser to use as your client certificate and key. If you are using firefox, Tools->Options->Advanced->Encryption->View Certificates. Click on My Certificates and import the certificate (client1.p12) created in the previous step.

Repeat steps 13-16 as often as required.

Step 17.  Enable client certificate authentication in Tomcat.  Open up
conf/server.xml and search for the text "clientAuth".  Set the value of the
attribute to "true". (In the case of Firebird, locate this setting in configuration\config.ini)

# SETTING UP A KEYSTORE FOR SSL IN A JAVA PROGRAM
----------------------------------------------------------------------------------------------------------------------------------

Step 13.  Create a keystore for your client program.
`keytool -genkey -alias client -keyalg RSA -keysize 2048 -keystore ssl/client/client.ks -storetype JCEKS`

and enter a simple password (ie `changeit`). The keysize can be 512, 2048, etc. The alias can be anything you want. Proceed to fill in the certificate subject information. If you don't want to be prompted for the subject information add the `-dname` parameter followed by a comma separate quoted string of all the subject name value pairs:

`keytool -genkey -alias client -keyalg RSA -keysize 2048 -keystore ssl/client/client.ks -storetype JCEKS -dname "C=US,ST=California,L=San Diego,O=Motorola,OU=<Server Type>,OU=<MSO Site ID>,CN=<UUID>"`

Step 14.  Create a client certificate request:
`keytool -certreq -keyalg RSA -alias client -file ssl/client/client.csr -keystore ssl/client/client.ks -storetype JCEKS`
The keysize can also be changed to 1024, 2048, etc.

Step 15.  Have your CA sign your client certificate (This example will create a signed certificate that is valid for 365 days):
`openssl x509 -CA ssl/ca/ca.pem -CAkey ssl/ca/ca.key -CAserial ssl/ca/ca.srl -req -in ssl/client/client.csr -out ssl/client/clientsignedbyCA.cer -days 365`

Step 16.  Import the signed certificate into your client keystore to use as your client certificate and key:
`keytool -import -alias client -storepass changeit -file ssl/client/clientsignedbyCA.cer -keystore ssl/client/client.ks -trustcacerts -storetype JCEKS`

A message "Certiificate reply was installed in keystore" is displayed

If you get an error in this step, it means that the keytool couldn't find the CA Certificate that you created earlier. If you didn't import the CA Certificate into the system wide JDK certificate authorities keystore, you can optionally import it into your local client keystore with the following command:

`keytool -import -alias client -storepass changeit -file ssl/ca/ca.pem -alias my_ca`

Repeat step 16 to import the signed certificate into your client keystore.

Step 17.  Verify the contents of your client keystore.
`keytool -list -storetype JCEKS -keystore ssl/client/client.ks -storepass changeit` or
`keytool -list -v -storetype JCEKS -keystore ssl/client/client.ks -storepass changeit` (verbose)

Step 18.  Enable client certificate authentication in Tomcat.  Open up
conf/server.xml and search for the text "clientAuth".  Set the value of the
attribute to "true". (In the case of Firebird, locate this setting in configuration\config.ini)

# SETTING UP A CERTIFICATE REQUEST WITH A VENDOR INDEX FIELD (NUMERIC OU)
---
Substitute the following in to the normal processes:

Step 3: Create the certificate request with two OU fields, as well as the private key.
`openssl req -new -newkey rsa:2048 -nodes -out ssl/ca/ca.csr -keyout ssl/ca/ca.key -subj "/CN=somedomain/O=My Corporation/OU=[Numeric Value]/OU=MYORG"`
